package com.toastworks.podimio.core;

import java.util.ArrayList;
import java.util.List;

import android.content.SharedPreferences;

import com.toastworks.podimio.interfaces.PlaylistRepository;
import com.toastworks.podimio.model.PlayListState;

public class PlayListManager {
	private static final String CURRENTLY_PLAYING_KEY = "com.toastworks.podimo.playing.url";
	private static final String PLAYBACKSTATE_IMAGE_KEY = "com.toastworks.podimo.playbackstate.image";
	private String mPlayingUrl = null;
	private String mPlaybackStateImage = null;
	private PlaylistRepository mRepository;
	private List<String> currentEpisodeList;
	private ArrayList<String> newEpisodeList;
	private int currentEpisode;
	private int episodesEqualUpToIndex = 0;

	public PlayListState getRevisedPlayListState(
			PlayListState currentPlayListState, PlaylistRepository repository) {
		mRepository = repository;
		currentEpisodeList = currentPlayListState.getEpisodeList();
		currentEpisode = currentPlayListState.getCurrentEpisode();
		newEpisodeList = mRepository.getPlayListUrls();
		episodesEqualUpToIndex = 0;

		if (listsAreEqual()) {
			return currentPlayListState;
		}

		else {
			if (currentEpisodeIsPresentInNewList()) {
				return new PlayListState(newEpisodeList,
						getNewEpisodeIndexFromCurrentEpisode());
			} else
				return new PlayListState(newEpisodeList,
						getNewEpisodeIndexFromPreviousEpisodes());
		}

	}

	private boolean listsAreEqual() {

		if (currentEpisodeList.size() == newEpisodeList.size())
			return true;

		for (int i = 0; i < currentEpisodeList.size(); i++) {
			if (!currentEpisodeList.get(i).equals(newEpisodeList.get(i))) {
				episodesEqualUpToIndex = i == 0 ? 0 : i - 1;
				return false;
			}
		}

		return true;

	}

	private boolean currentEpisodeIsPresentInNewList() {
		for (String item : newEpisodeList) {
			if (item.equals(currentEpisodeList.get(currentEpisode)))
				return true;
		}
		return false;
	}

	// assume that user hasn't removed and re-added episode that has just
	// finished
	// play from subsequent episode
	private int getNewEpisodeIndexFromCurrentEpisode() {
		int newEpisodeIndex = 0;

		for (String item : newEpisodeList) {
			if (item.equals(currentEpisodeList.get(currentEpisode))) {

				// play next episode if it exists, otherwise return to the
				// earliest new entry
				newEpisodeIndex = newEpisodeList.indexOf(item) + 1 < newEpisodeList
						.size() ? newEpisodeList.indexOf(item)
						: episodesEqualUpToIndex;
				break;
			}
		}

		return newEpisodeIndex;
	}

	private int getNewEpisodeIndexFromPreviousEpisodes() {
		int newEpisodeIndex = 0;

		if (currentEpisodeList.size() < newEpisodeList.size()) {

			for (int i = 0; i < currentEpisodeList.size(); i++) {
				if (currentEpisodeList.get(i).equals(newEpisodeList.get(i))) {
					episodesEqualUpToIndex = i;
				} else
					break;
			}
		}

		if (currentEpisodeList.size() > newEpisodeList.size()) {

			for (int i = 0; i < newEpisodeList.size(); i++) {
				if (newEpisodeList.get(i).equals(currentEpisodeList.get(i))) {
					episodesEqualUpToIndex = i;
				} else
					break;
			}
		}

		return newEpisodeIndex;
	}
	
	public String getCurrentlyPlayingUrl(SharedPreferences prefs){
		
		if(currentlyPlayingUrlExists(prefs)) return mPlayingUrl;
		else return null;
	}
	
	public void storeCurrentlyPlayingUrl(SharedPreferences prefs, String value){
		prefs.edit().putString(CURRENTLY_PLAYING_KEY, value).commit();
		mPlayingUrl = value;
	}
	
	public void deleteCurrentlyPlayingUrl(SharedPreferences prefs){
		prefs.edit().remove(CURRENTLY_PLAYING_KEY).commit();
		mPlayingUrl = null;
	}
	
	public boolean currentlyPlayingUrlExists(SharedPreferences prefs){

		mPlayingUrl = prefs.getString(CURRENTLY_PLAYING_KEY, null);
		
		if(mPlayingUrl != null) return true;
		else return false;
	}
	
	public void storePlaybackStateImage(SharedPreferences prefs, String value){
		prefs.edit().putString(PLAYBACKSTATE_IMAGE_KEY, value).commit();
		mPlaybackStateImage = value;
	}
	
	public String getPlaybackStateImage(SharedPreferences prefs){
		
		if(PlaybackStateImageExists(prefs)) return mPlaybackStateImage;
		else return null;
	}
	
	public boolean PlaybackStateImageExists(SharedPreferences prefs){

		mPlaybackStateImage = prefs.getString(PLAYBACKSTATE_IMAGE_KEY, null);
		
		if(mPlaybackStateImage != null) return true;
		else return false;
	}
	

	
}
