package com.toastworks.podimio.core;

import org.apache.http.client.methods.HttpRequestBase;

import android.content.SharedPreferences;

public class TokenManager {
	
	private static final String TOKEN_KEY = "com.toastworks.podimo.token";
	private String _token = null;
	
	public boolean tokenExists(SharedPreferences prefs){

		_token = prefs.getString(TOKEN_KEY, null);
		
		if(_token != null) return true;
		else return false;
	}
	
	public String getToken(SharedPreferences prefs){
		
		if(tokenExists(prefs)) return _token;
		else return null;
	}
	
	public void storeToken(SharedPreferences prefs, String value){
		prefs.edit().putString(TOKEN_KEY, value).commit();
		_token = value;
	}
	
	public void deleteToken(SharedPreferences prefs){
		prefs.edit().remove(TOKEN_KEY).commit();
		_token = null;
	}
	
	public void putTokenInRequest(SharedPreferences prefs, HttpRequestBase request){
		
		TokenManager tokenManager = new TokenManager();
		request.setHeader("Authorization", "Bearer "+tokenManager.getToken(prefs));
			
	}
	

}
