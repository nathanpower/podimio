package com.toastworks.podimio.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.widget.ProgressBar;

import com.toastworks.podimio.asynctasks.ServiceHandlerTask;
import com.toastworks.podimio.interfaces.AsyncServiceTaskListener;

public class AccountManager {
	
	private ProgressBar _progressBar;
	 private AsyncServiceTaskListener _listener;
	
	public AccountManager(AsyncServiceTaskListener listener, ProgressBar progressBar){
		_progressBar = progressBar;
		_listener = listener;
	}
	
	public void SignIn(String username, String password){
		ServiceHandlerTask asyncHandler = new ServiceHandlerTask(_listener, _progressBar);
		String URL = "https://podcastapi.azurewebsites.net/token";
		// Create a new HttpClient and Post Header

		HttpPost httppost = new HttpPost(URL);


		try {
		    // Add your data
		    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
		    nameValuePairs.add(new BasicNameValuePair("grant_type", "password"));
		    nameValuePairs.add(new BasicNameValuePair("username", username));
		    nameValuePairs.add(new BasicNameValuePair("password", password));
		    
		    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            

		    // Execute HTTP Post Request
		    asyncHandler.execute(httppost);

		} catch (IOException e) {
			System.out.print(e.getMessage());
		} 
	
	}
	
	public void register(String username, String password, String confirmPassword){
		ServiceHandlerTask asyncHandler = new ServiceHandlerTask(_listener, _progressBar);
		String URL = "https://podcastapi.azurewebsites.net/api/account/register";
        JSONObject json = new JSONObject();

         try {
             HttpPost post = new HttpPost(URL);
             json.put("UserName", username);
             json.put("Password", password);
             json.put("ConfirmPassword", confirmPassword);
             StringEntity se = new StringEntity( json.toString());  
             se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
             post.setEntity(se);
             
             asyncHandler.execute(post);

         } catch(Exception e) {
        	 System.out.print(e.getMessage());
         }
         
	}
	
	
	
}
