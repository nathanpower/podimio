package com.toastworks.podimio.core;

import android.content.SharedPreferences;

public class RevisionManager {
	
	private static final String REVISION_KEY = "com.toastworks.podimo.subrevision";
	private String _revision = null;
	
	public String getSubscriptionRevision(SharedPreferences prefs){
		
		_revision = prefs.getString(REVISION_KEY, null);
		
		if(_revision != null) return _revision;
		else return null;

	}
	
	public void updateRevision(SharedPreferences prefs, String value){
		prefs.edit().putString(REVISION_KEY, value).commit();
		_revision = value;
	}
	
}