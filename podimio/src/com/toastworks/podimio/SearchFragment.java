package com.toastworks.podimio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.toastworks.podimio.asynctasks.ChannelJsonParserTask;
import com.toastworks.podimio.asynctasks.ServiceCallInitialiserTask;
import com.toastworks.podimio.asynctasks.ServiceHandlerTask;
import com.toastworks.podimio.interfaces.AsyncChannelParseTaskListener;
import com.toastworks.podimio.interfaces.AsyncServiceTaskListener;
import com.toastworks.podimio.interfaces.AsyncSvcCallInitListener;
import com.toastworks.podimio.interfaces.ListViewSelectionHighlighter;
import com.toastworks.podimio.interfaces.SubscriptionManager;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimio.model.FeedChannelJsonMapper;
import com.toastworks.podimio.model.FeedItemDao;
import com.toastworks.podimio.model.HttpResponseViewModel;
import com.toastworks.podimio.model.RequestMethod;
import com.toastworks.podimio.model.ServiceRequest;
import com.toastworks.podimo.R;

public class SearchFragment extends ListFragment implements
		AsyncServiceTaskListener, AsyncChannelParseTaskListener,
		AsyncSvcCallInitListener, ListViewSelectionHighlighter {
	private View mRootView;
	private LayoutInflater mInflater;
	private SharedPreferences mPrefs;
	private List<FeedChannelJsonMapper> mResultsList = new ArrayList<FeedChannelJsonMapper>();
	List<Map<String, String>> mResultsViewData = new ArrayList<Map<String, String>>();
	private SimpleAdapter mAdapter;
	private SubscriptionManager mSubscriptionManager;
	private ToastMaker mToastMaker;
	private static final String RESULTS_VIEW_KEY = "SearchResultsView";
	private static final String RESULTS_KEY = "SearchResults";
	private FeedChannelJsonMapper mSelectedChannel;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mToastMaker = (ToastMaker) activity;
			mSubscriptionManager = (SubscriptionManager) activity;
		} catch (ClassCastException castException) {
			castException.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mInflater = inflater;
		mRootView = inflater.inflate(R.layout.fragment_main_search, container,
				false);
		mPrefs = getActivity().getSharedPreferences("com.toastworks.podimo",
				Context.MODE_PRIVATE);
		setHasOptionsMenu(true);

		
		 if(savedInstanceState != null){
			 if(savedInstanceState.getSerializable(RESULTS_VIEW_KEY) != null) {
				 mResultsViewData = (List<Map<String, String>>) savedInstanceState.
				 getSerializable(RESULTS_KEY);
				 mResultsList = savedInstanceState.getParcelableArrayList(RESULTS_VIEW_KEY);			 
			 }
			 
		 }
		
		EditText searchField = (EditText) getSearchField();
		searchField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
		            processSearch();
		            return true;
		        }
		        return false;
		    }
		});

		return mRootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAdapter = new SimpleAdapter(mInflater.getContext(), mResultsViewData,
				R.layout.podcast_list_item_2, new String[] { "title",
						"description" }, new int[] { android.R.id.text1,
						android.R.id.text2 });

		setListAdapter(mAdapter);

		ListView listView = getListView();
		registerForContextMenu(listView);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                int position, long id) {

        		mSelectedChannel = mResultsList.get(position);
            	getActivity().invalidateOptionsMenu();
            	toggleSelection((ListView) parent, view);
          	  	
            }});
		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    // Inflate the menu items for use in the action bar
	    inflater.inflate(R.menu.search_fragment_actions, menu);
		if(mSelectedChannel != null)
			menu.findItem(R.id.action_add_to_subscriptions).setVisible(true);
		else menu.findItem(R.id.action_add_to_subscriptions).setVisible(false);
	    
	}
	
	

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onPrepareOptionsMenu(menu);

					
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		getActivity().getMenuInflater().inflate(R.menu.search_item_menu, menu);
	}
	
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_to_subscriptions:
			subscribeToChannel(mSelectedChannel);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.add_to_subscriptions:
			subscribeToChannel(item);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable(RESULTS_KEY, (Serializable) mResultsViewData);
		outState.putParcelableArrayList(RESULTS_VIEW_KEY, (ArrayList<? extends Parcelable>) mResultsList);
        super.onSaveInstanceState(outState);
    }

	private void subscribeToChannel(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		
		FeedChannelJsonMapper channel = mResultsList.get(info.position);
		mSubscriptionManager.addSubscription(channel);
	}
	
	private void subscribeToChannel(FeedChannelJsonMapper channel) {
		mSubscriptionManager.addSubscription(channel);
		mResultsViewData.clear();
		mAdapter.notifyDataSetChanged();
		mSelectedChannel = null;
		getActivity().invalidateOptionsMenu();
	}

	protected void processSearch() {
		mResultsList.clear();
		mResultsViewData.clear();

		String url = "https://podcastapi.azurewebsites.net/breeze/Breeze/search";
		ProgressBar progressBar = getProgressBar();
		ServiceCallInitialiserTask svcInitialiser = new ServiceCallInitialiserTask(
				this, mPrefs);
		final EditText searchField = getSearchField();
		String query = searchField.getText().toString();
		ServiceRequest request = new ServiceRequest(RequestMethod.GET, url);

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("query", query));
		request.setmParams(params);

		progressBar.setVisibility(View.VISIBLE);
		getSearchField().setText("");
		svcInitialiser.execute(request);

	}

	// Callback from service initialiser when completed
	@Override
	public void onTaskCompleted(HttpRequestBase request) {
		ServiceHandlerTask svcHandler = new ServiceHandlerTask(this, null);
		svcHandler.execute(request);
	}

	// Callback from service call when completed
	@Override
	public void onTaskCompleted(HttpResponseViewModel response) {

		if (response.getResponseCode() != 200) {
			mToastMaker.makeToast("No results found");
			getProgressBar().setVisibility(View.GONE);

		} else {
			ChannelJsonParserTask parser = new ChannelJsonParserTask(this,
					getProgressBar());
			parser.execute(response.getResponseBody());
		}

	}

	// Callback from parser when completed
	@Override
	public void onTaskCompleted(FeedChannelJsonMapper channel) {
		mResultsList.add(channel);
		if(channel != null){
			// hide virtual keyboard
			hideKeyboard();

			mapToJson();
		}
		else mToastMaker.makeToast("No results found");
	}

	private void mapToJson() {
		for (FeedChannelJsonMapper item : mResultsList) {
			Map<String, String> datum = new HashMap<String, String>(2);
			datum.put("title", item.getTitle());
			datum.put("description", item.getDescription().toString());
			mResultsViewData.add(datum);
		}
	}

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager)getActivity().
				getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getSearchField().getWindowToken(), 0);
	}

	private ProgressBar getProgressBar() {
		return (ProgressBar) mRootView.findViewById(R.id.search_progress_bar);
	}
	
	
	private EditText getSearchField() {
		return (EditText) mRootView.findViewById(R.id.search_field);
	}

	@Override
	public void toggleSelection(ListView parent, View view) {
		if(parent == null)
			parent = getListView();
		
		for (int j = 0; j < parent.getChildCount(); j++)
            parent.getChildAt(j).setBackgroundColor(Color.parseColor("#5a5a5a"));

        if(view != null)
        	view.setBackgroundColor(Color.parseColor("#3a3a3a"));
	}
	
	

}