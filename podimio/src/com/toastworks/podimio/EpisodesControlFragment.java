package com.toastworks.podimio;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.toastworks.podimio.interfaces.EpisodeSelectedListener;
import com.toastworks.podimio.interfaces.FragmentManipulator;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimo.R;
import com.toastworks.utilities.Utilities;

public class EpisodesControlFragment extends Fragment{
	private View mRootView;
	private ToastMaker mToastMaker;
	private FragmentManipulator mFragmentManipulator;
	private EpisodeSelectedListener mEpisodesManager;
	private Utilities mUtils;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		 try {
			 mFragmentManipulator = (FragmentManipulator) activity;
			 mEpisodesManager = (EpisodeSelectedListener) activity;
			 mToastMaker = (ToastMaker) activity;
         } catch (ClassCastException castException) {
        	 castException.printStackTrace();
         }
	}
	
	@Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	      Bundle savedInstanceState) {
		mUtils = new Utilities();
	    mRootView = inflater.inflate(R.layout.fragment_episode_control,
	        container, false);
	    
	    ImageButton playButton = getPlayButton();
    	playButton.setOnClickListener(new View.OnClickListener() {
    	    @Override
    	    public void onClick(View v) {
    	    	EpisodesControlFragment.this.playEpisode();
    	    }
    	});
    	
    	ImageButton addButton = getAddButton();
    	addButton.setOnClickListener(new View.OnClickListener() {
    	    @Override
    	    public void onClick(View v) {
    	    	EpisodesControlFragment.this.addEpisodeToPlayList(v);
    	    }
    	});
    	
    	ImageButton closeButton = getCloseButton();
    	closeButton.setOnClickListener(new View.OnClickListener() {
    	    @Override
    	    public void onClick(View v) {
    	    	EpisodesControlFragment.this.closeDetailsFragments();
    	    }
    	});
	        
	    return mRootView;
	  }

	private ImageButton getCloseButton() {
		return (ImageButton) mRootView.findViewById(R.id.episode_close);
	}

	private ImageButton getAddButton() {
		return (ImageButton) mRootView.findViewById(R.id.episode_add);
	}

	private ImageButton getPlayButton() {
		return (ImageButton) mRootView.findViewById(R.id.episode_play);
	}

	protected void closeDetailsFragments() {
		mFragmentManipulator.hideFragments();
	}

	protected void addEpisodeToPlayList(View v) {
		mEpisodesManager.addEpisodeToPlayList(v);	
	}

	protected void playEpisode() {
		if(mUtils.haveNetworkConnection(getActivity()))
			mEpisodesManager.playEpisode();	
		else mToastMaker.makeToast("No internet connection available - streaming not possible");
	}
	
	
	
	
	
	

}
