package com.toastworks.podimio.persistence;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.toastworks.podimio.interfaces.PlaylistRepository;
import com.toastworks.podimio.model.FeedChannelDao;
import com.toastworks.podimio.model.FeedChannelJsonMapper;
import com.toastworks.podimio.model.FeedItemDao;
import com.toastworks.podimio.model.FeedItemJsonMapper;
import com.toastworks.podimio.model.PlaylistItemViewModel;

public class PodcastRepository implements PlaylistRepository{

	  // Database fields
	  private SQLiteDatabase database;
	  private DatabaseHelper dbHelper;
	  private String[] allFeedColumns = { DatabaseHelper.FEED_URL, DatabaseHelper.FEED_TITLE,
	      DatabaseHelper.FEED_DESCRIPTION };
	  private String[] feedChannelUrl = { DatabaseHelper.FEED_URL };
	  private String[] allItemColumns = { DatabaseHelper.ITEM_URL, DatabaseHelper.ITEM_TITLE,
		      DatabaseHelper.ITEM_DESCRIPTION, DatabaseHelper.ITEM_FEEDURL };
	  
	  private String[] allPlaylistColumns = { DatabaseHelper.PLAYLIST_EPISODE_URL, DatabaseHelper.PLAYLIST_FEED_TITLE,
		      DatabaseHelper.PLAYLIST_EPISODE_TITLE };

	  public PodcastRepository(Context context) {
	    dbHelper = new DatabaseHelper(context);
	  }

	  public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	  }

	  public void close() {
	    dbHelper.close();
	  }
	  
	  public boolean FeedRecordExists(String url) {
		  open();
		    String Query = "Select * from " + DatabaseHelper.FEED_TABLE_NAME 
		    		+ " where " + DatabaseHelper.FEED_URL + "="
		            + "\"" + url + "\"";
		    Cursor cursor = database.rawQuery(Query, null);
		            if(cursor.getCount() == 0){
					    return false;
					  }
		   close();         
		        return true;
		}

	  public void insertChannel(FeedChannelJsonMapper channel) {
		open();
	    ContentValues feedValues = new ContentValues();
	    ContentValues itemValues;
	    feedValues.put(DatabaseHelper.FEED_URL, channel.getUrl());
	    feedValues.put(DatabaseHelper.FEED_TITLE, channel.getTitle());
	    feedValues.put(DatabaseHelper.FEED_DESCRIPTION, channel.getDescription());
	    database.insert(DatabaseHelper.FEED_TABLE_NAME, null,
	        feedValues);
	    
	    for(FeedItemJsonMapper item : channel.getItems()){
	    	itemValues = new ContentValues();
	    	itemValues.put(DatabaseHelper.ITEM_URL, item.getUrl());
	    	itemValues.put(DatabaseHelper.ITEM_FEEDURL, item.getFeedUrl());
	    	itemValues.put(DatabaseHelper.ITEM_TITLE, item.getTitle());
	    	itemValues.put(DatabaseHelper.ITEM_DESCRIPTION, item.getDescription());
	    	database.insert(DatabaseHelper.ITEM_TABLE_NAME, null,
	    	        itemValues);	    	
	    }
	    
	   close();
	   
	  }
	  
	  public void insertPlayListItem(PlaylistItemViewModel item) {
			open();
		    ContentValues playlistItemValues = new ContentValues();
		    playlistItemValues.put(DatabaseHelper.PLAYLIST_EPISODE_URL, item.getUrl());
		    playlistItemValues.put(DatabaseHelper.PLAYLIST_FEED_TITLE, item.getFeedTitle());
		    playlistItemValues.put(DatabaseHelper.PLAYLIST_EPISODE_TITLE, item.getEpisodeTitle());
		    database.insert(DatabaseHelper.PLAYLIST_TABLE_NAME, null,
		        playlistItemValues);
		    
		   close();
		   
		  }

	  public List<PlaylistItemViewModel> getPlayList() {
		  open();
		  List<PlaylistItemViewModel> playlist = new ArrayList<PlaylistItemViewModel>();

		    Cursor cursor = database.query(DatabaseHelper.PLAYLIST_TABLE_NAME,
		        allPlaylistColumns, null, null, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      PlaylistItemViewModel playlistItem = cursorToPlaylistItem(cursor);
		      playlist.add(playlistItem);
		      cursor.moveToNext();
		    }

		    cursor.close();
		    close();
		    return playlist;
		  
	  }
	  
	  public ArrayList<String> getPlayListUrls() {
		  open();
		  ArrayList<String> playlist = new ArrayList<String>();

		    Cursor cursor = database.query(DatabaseHelper.PLAYLIST_TABLE_NAME,
		        allPlaylistColumns, null, null, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      PlaylistItemViewModel playlistItem = cursorToPlaylistItem(cursor);
		      playlist.add(playlistItem.getUrl());
		      cursor.moveToNext();
		    }

		    cursor.close();
		    close();
		    return playlist;
		  
	  }
	  
	  public List<FeedChannelDao> getAllChannels() {
		  
		open();
	    List<FeedChannelDao> channels = new ArrayList<FeedChannelDao>();

	    Cursor cursor = database.query(DatabaseHelper.FEED_TABLE_NAME,
	        allFeedColumns, null, null, null, null, null);

	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      FeedChannelDao channel = cursorToChannel(cursor);
	      channels.add(channel);
	      cursor.moveToNext();
	    }

	    cursor.close();
	    close();
	    return channels;
	  }
	  
	  public List<String> getAllChannelUrls() {
		  
			open();
		    List<String> channelUrls = new ArrayList<String>();

		    Cursor cursor = database.query(DatabaseHelper.FEED_TABLE_NAME,
		    		allFeedColumns, null, null, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      String channelUrl = cursorToChannel(cursor).getUrl();
		      channelUrls.add(channelUrl);
		      cursor.moveToNext();
		    }

		    cursor.close();
		    close();
		    return channelUrls;
		  }
	  
	  public PlaylistItemViewModel getEpisodeForUrl(String url) {
		  open();
		    FeedItemDao episode = null;
		    FeedChannelDao channel = null;
		    PlaylistItemViewModel episodeViewModel = null;
		    String[] itemTableArgs = { url };
		    

		    Cursor episodeCursor = database.query(DatabaseHelper.ITEM_TABLE_NAME,
		        allItemColumns, "url=?", itemTableArgs, null, null, null);

		    episodeCursor.moveToFirst();
		    while (!episodeCursor.isAfterLast()) {
		      episode = cursorToItem(episodeCursor);
		      episodeCursor.moveToNext();
		    }

		    
		    episodeCursor.close();
		    
		    if(episode == null) { // look in playlist table
		    	    episodeCursor = database.query(DatabaseHelper.PLAYLIST_TABLE_NAME,
				        allPlaylistColumns, "pl_ep_url=?", itemTableArgs, null, null, null);

				    episodeCursor.moveToFirst();
				    while (!episodeCursor.isAfterLast()) {
				      episodeViewModel = cursorToPlaylistItem(episodeCursor);
				      episodeCursor.moveToNext();
				    }
				    episodeCursor.close();
		    }
		    else {
				    String [] channelTableArgs = { episode.getFeedUrl() };
				    
				    Cursor channelCursor = database.query(DatabaseHelper.FEED_TABLE_NAME,
					        allFeedColumns, "url=?", channelTableArgs, null, null, null);
				    
				    channelCursor.moveToFirst();
				    while (!channelCursor.isAfterLast()) {
					      channel = cursorToChannel(channelCursor);
					      channelCursor.moveToNext();
					    }
				    
				    channelCursor.close();
				    episodeViewModel = new PlaylistItemViewModel(episode, channel.getTitle());
		    }
		    close();
		    
		    
		    
		    return episodeViewModel;
		  }
	  
	  public List<FeedItemDao> getAllItemsForFeed(String feedUrl) {
		  open();
		    List<FeedItemDao> items = new ArrayList<FeedItemDao>();
		    String[] args = { feedUrl };
		    

		    Cursor cursor = database.query(DatabaseHelper.ITEM_TABLE_NAME,
		        allItemColumns, "feedUrl=?", args, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      FeedItemDao channel = cursorToItem(cursor);
		      items.add(channel);
		      cursor.moveToNext();
		    }

		    cursor.close();
		    close();
		    return items;
		  }

	  private FeedItemDao cursorToItem(Cursor cursor) {
		  FeedItemDao item = new FeedItemDao();
		    item.setUrl(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ITEM_URL)));
		    item.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ITEM_TITLE)));
		    item.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ITEM_DESCRIPTION)));
		    item.setFeedUrl(cursor.getString(cursor.getColumnIndex(DatabaseHelper.ITEM_FEEDURL)));
		    return item;
	}

	private FeedChannelDao cursorToChannel(Cursor cursor) {
	    FeedChannelDao channel = new FeedChannelDao();
	    channel.setUrl(cursor.getString(cursor.getColumnIndex(DatabaseHelper.FEED_URL)));
	    channel.setTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.FEED_TITLE)));
	    channel.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.FEED_DESCRIPTION)));
	    return channel;
	  }
	
	private PlaylistItemViewModel cursorToPlaylistItem(Cursor cursor) {
	    PlaylistItemViewModel item = new PlaylistItemViewModel();
	    item.setUrl(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLAYLIST_EPISODE_URL)));
	    item.setFeedTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLAYLIST_FEED_TITLE)));
	    item.setEpisodeTitle(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLAYLIST_EPISODE_TITLE)));
	    return item;
	  }
	
	public void dropTables(){
		open();
		database.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.FEED_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.ITEM_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.PLAYLIST_TABLE_NAME);
        close();
	}

	public void deleteChannel(String channel) {
		open();
		database.delete(DatabaseHelper.ITEM_TABLE_NAME, 
				DatabaseHelper.ITEM_FEEDURL + " = ?", 
	            new String[] {channel});
		database.delete(DatabaseHelper.FEED_TABLE_NAME, 
				DatabaseHelper.FEED_URL + " = ?", 
	            new String[] {channel});
		
		close();
	}
	
	public void deletePlaylistItem(PlaylistItemViewModel item) {
		open();
		database.delete(DatabaseHelper.PLAYLIST_TABLE_NAME, 
				DatabaseHelper.PLAYLIST_EPISODE_URL + " = ?", 
	            new String[] {item.getUrl()});
	
		close();
	}

	} 
