package com.toastworks.podimio.persistence;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseAdapter {
    //Table name
    private static final String FEED_TABLE = "feed";
    private static final String ITEM_TABLE = "item";
    //Table unique id
    public static final String FEED_PK = "url";
    public static final String ITEM_PK = "url";


    private Context context;
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    public DatabaseAdapter(Context context) {
        this.context = context;
    }

    public DatabaseAdapter open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }
    
  
}