package com.toastworks.podimio.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    // The database name and version
    public static final String DB_NAME = "podimo.db";
    public static final int DB_VERSION = 2;
    public static final String FEED_TABLE_NAME = "feed";
    public static final String ITEM_TABLE_NAME = "item";
    public static final String PLAYLIST_TABLE_NAME = "playlist";
    public static final String FEED_URL = "url";
    public static final String FEED_TITLE = "title";
    public static final String FEED_DESCRIPTION = "description";
    public static final String ITEM_URL = "url";
    public static final String ITEM_TITLE = "title";
    public static final String ITEM_DESCRIPTION = "description";
    public static final String ITEM_FEEDURL = "feedurl";
    public static final String PLAYLIST_EPISODE_URL = "pl_ep_url";
    public static final String PLAYLIST_FEED_TITLE = "pl_feed_title";
    public static final String PLAYLIST_EPISODE_TITLE = "pl_ep_title";
    
    
    // The database user table
    private static final String DB_TABLE_FEED_CREATE = "create table " + FEED_TABLE_NAME 
    							+ " (" + FEED_URL + " text primary key, " 
                                + FEED_TITLE + " text not null, " + FEED_DESCRIPTION + 
                                " text not null);";
    
    private static final String DB_TABLE_ITEM_CREATE = "create table " + ITEM_TABLE_NAME 
    							+ "(" + ITEM_URL + " text primary key, " 
    							+ ITEM_FEEDURL + " text not null, " + ITEM_TITLE + 
    							" text not null, " + ITEM_DESCRIPTION + " text not null);";
    
    private static final String DB_TABLE_PLAYLIST_CREATE = "create table " + PLAYLIST_TABLE_NAME 
			+ "(" + PLAYLIST_EPISODE_URL + " text primary key, " 
			+ PLAYLIST_FEED_TITLE + " text not null, " + PLAYLIST_EPISODE_TITLE + 
			" text not null);";
    /**
     * Database Helper constructor. 
     * @param context
     */
    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    /**
     * Creates the database tables.
     */
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DB_TABLE_FEED_CREATE);
        database.execSQL(DB_TABLE_ITEM_CREATE);
        database.execSQL(DB_TABLE_PLAYLIST_CREATE);
    }
    /**
     * Handles the table version and the drop of a table.   
     */         
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading databse from version" + oldVersion + "to " 
                + newVersion + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + FEED_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + ITEM_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_TABLE_NAME);
        
        onCreate(database);
    }
}
