package com.toastworks.podimio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.toastworks.podimio.core.AccountManager;
import com.toastworks.podimio.interfaces.AccountInputValidator;
import com.toastworks.podimio.interfaces.AsyncServiceTaskListener;
import com.toastworks.podimio.interfaces.NetworkChecker;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimio.model.HttpResponseViewModel;
import com.toastworks.podimo.R;

public class RegisterFragment extends Fragment implements AsyncServiceTaskListener {

	private View mRootView;
	private ToastMaker mToastMaker;
	private AccountInputValidator mValidator;
	private NetworkChecker mNetworkChecker;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		 try {
             mToastMaker = (ToastMaker) activity;
             mValidator = (AccountInputValidator) activity;
             mNetworkChecker = (NetworkChecker) activity;
         } catch (ClassCastException castException) {
        	 castException.printStackTrace();
         }
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        mRootView = inflater.inflate(R.layout.fragment_register, container, false);
         
        Button button = getRegisterButton();
    	button.setOnClickListener(new View.OnClickListener() {
    	    @Override
    	    public void onClick(View v) {
    	       processRegistration();
    	    }
    	});
        
        return mRootView;
    }
	private Button getRegisterButton() {
		return (Button) mRootView.findViewById(R.id.register);
	}

	protected void processRegistration() {
		ProgressBar progressBar = (ProgressBar) mRootView.findViewById(R.id.register_progress_bar);	
		
		AccountManager accountManager = new AccountManager(this, progressBar);
		final EditText nameField = getUsernameField();  
		String username = nameField.getText().toString();  
		final EditText passwordField = getPasswordField();  
		String password = passwordField.getText().toString();  
		final EditText confirmPasswordField = getConfirmPasswordField();  
		String confirmPassword = confirmPasswordField.getText().toString();  
		boolean haveNetworkConnection = mNetworkChecker.haveNetworkConnection();
		boolean inputIsValid = mValidator.inputIsValid(username, password, confirmPassword);
		
		if(haveNetworkConnection && inputIsValid)
		{	
			getUsernameField().setText("");
			getPasswordField().setText("");
			getConfirmPasswordField().setText("");
			getRegisterButton().setEnabled(false);
			progressBar.setVisibility(View.VISIBLE);
			accountManager.register(username, password, confirmPassword);	
		}
	}
	private EditText getConfirmPasswordField() {
		return (EditText) mRootView.findViewById(R.id.confirm_password);
	}
	private EditText getPasswordField() {
		return (EditText) mRootView.findViewById(R.id.register_password);
	}
	private EditText getUsernameField() {
		return (EditText) mRootView.findViewById(R.id.register_username);
	}
	
	
	// Callback when registration complete
	@Override
	public void onTaskCompleted(HttpResponseViewModel response) {
		Intent intent;
		getRegisterButton().setEnabled(true);
		if(response.getResponseCode() == 200) {
			mToastMaker.makeToast("User successfully registered");
			 intent = new Intent(getActivity().getApplicationContext(), AccountActivity.class);
			 startActivity(intent);
		}
		
		else{
			mToastMaker.makeToast("Error: " + response.getResponseCodeString());
		}
	}
	
	
	

}
