package com.toastworks.podimio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.toastworks.podimio.interfaces.EpisodeManager;
import com.toastworks.podimio.interfaces.EpisodeSelectedListener;
import com.toastworks.podimio.interfaces.FragmentManipulator;
import com.toastworks.podimio.interfaces.ListViewSelectionHighlighter;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimio.model.FeedItemDao;
import com.toastworks.podimio.services.SyncService;
import com.toastworks.podimo.R;

public class EpisodesActivity extends FragmentActivity implements EpisodeSelectedListener, 
										FragmentManipulator, ToastMaker{

	private FeedItemDao mSelectedEpisode;
	private boolean mShowFragments = false;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_episodes);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		if(savedInstanceState != null) {
			mSelectedEpisode = savedInstanceState.getParcelable("SelectedEpisode");
			mShowFragments = savedInstanceState.getBoolean("ShowFragments");
		}
									  
	}
	
		
	@Override
	protected void onResumeFragments() {
		// TODO Auto-generated method stub
		super.onResumeFragments();
		if(mSelectedEpisode != null && mShowFragments)
			onItemSelected(mSelectedEpisode);
		else hideFragments();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable("SelectedEpisode", mSelectedEpisode);
		outState.putBoolean("ShowFragments", mShowFragments);
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.episode_activity_actions, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    case R.id.open_playlist:
	    	openPlayList();
	    	return true;
	    case R.id.action_check_for_new_episodes:
	    	checkForNewEpisodes();
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}

	private void checkForNewEpisodes() {
		EpisodeManager fragment = (EpisodeManager)getEpisodesFragment();
		fragment.checkForNewEpisodes();
	}

	@Override
	public void onItemSelected(FeedItemDao item) {
		mSelectedEpisode = item;
		showEpisodeDetails();
		EpisodeDetailFragment fragment = (EpisodeDetailFragment) getEpisodeDetailFragment();
	        if (fragment != null && fragment.isInLayout()) {
	          fragment.setText(((FeedItemDao) item).getDescription(), ((FeedItemDao)item).getTitle());
	        } 
		
	}

	private void showEpisodeDetails() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();  
		ft.show(getEpisodeDetailFragment());  
		ft.show(getEpisodesControlFragment());
		ft.commit();
		mShowFragments = true;
	}
	
	@Override
	public void hideFragments(){
		/*ListViewSelectionHighlighter highlighter = (ListViewSelectionHighlighter) getEpisodesFragment();
		highlighter.toggleSelection(null, null);*/
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();  
		ft.hide(getEpisodeDetailFragment());  
		ft.hide(getEpisodesControlFragment());  
		ft.commit();
		mShowFragments = false;
	}
	
	private Fragment getEpisodesFragment() {
		EpisodesFragment fragment = (EpisodesFragment) getSupportFragmentManager()
	            .findFragmentById(R.id.episodesFragment);
		return fragment;
	}
	

	private Fragment getEpisodeDetailFragment() {
		EpisodeDetailFragment fragment = (EpisodeDetailFragment) getSupportFragmentManager()
	            .findFragmentById(R.id.episodeDetailFragment);
		return fragment;
	}
	
	private Fragment getEpisodesControlFragment() {
		EpisodesControlFragment fragment = (EpisodesControlFragment) getSupportFragmentManager()
	            .findFragmentById(R.id.episodesControlFragment);
		return fragment;
	}
	
	@Override
	public void playEpisode() {
		EpisodeManager ps = (EpisodeManager) getEpisodesFragment();
		ps.startPlayerForEpisode(mSelectedEpisode);
	}

	@Override
	public void addEpisodeToPlayList(View view) {
		EpisodeManager ps = (EpisodeManager) getEpisodesFragment();
		ps.addEpisodeToPlayList(mSelectedEpisode);
	}
	
	private void openPlayList() {
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.putExtra("GoToPlayList", true);	
		startActivity(intent);
		finish();
	}
	
	@Override
	public void makeToast(String message) {
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, message, duration);
		toast.show();
	}

	
}
