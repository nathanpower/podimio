package com.toastworks.podimio.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.toastworks.podimio.core.RevisionManager;
import com.toastworks.podimio.core.TokenManager;
import com.toastworks.podimio.model.FeedChannelJsonMapper;
import com.toastworks.podimio.model.HttpResponseViewModel;
import com.toastworks.podimio.persistence.PodcastRepository;

public class RefreshFeedService extends IntentService {
	private static final String CHECK_CHANNEL_URL = "https://podcastapi.azurewebsites.net/breeze/subscriptions/refreshchannel";
	PodcastRepository mDb;
	SharedPreferences mPrefs;
	String mChannelUrl;
	String mLatestEpisodeUrl;
	String mResultMessage;

	public static final String NOTIFICATION = "com.toastworks.podimio.refresh_receiver";
	
	public RefreshFeedService() {
		super("RefreshFeedService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		Context context = getApplicationContext();
		mPrefs = context.getSharedPreferences("com.toastworks.podimo",
				Context.MODE_PRIVATE);
		mDb = new PodcastRepository(getBaseContext());
		mChannelUrl = intent.getStringExtra("channelUrl");
		mLatestEpisodeUrl = intent.getStringExtra("episodeUrl");
		
		checkForUpdates();
		
		publishResults();
	}

	private void publishResults() {
		  Intent intent = new Intent(NOTIFICATION);
	      intent.putExtra("Result", mResultMessage);
	      sendBroadcast(intent);	
	}


	private void checkForUpdates() {
		FeedChannelJsonMapper channel = null;

	    try {
			channel = getChannel();
		} catch (ClientProtocolException e) {
			mResultMessage = "Error updating channel";
			e.printStackTrace();
		} catch (IOException e) {
			mResultMessage = "Error updating channel";
			e.printStackTrace();
		} catch (JSONException e) {
			mResultMessage = "Error updating channel";
			e.printStackTrace();
		}
		
	    if(channel != null){
		    mDb.deleteChannel(mChannelUrl);
			mDb.insertChannel(channel);
			mResultMessage = "Channel Updated Successfully";
	    }
	    
	    else mResultMessage = "Channel is already up to date";
	}

	private FeedChannelJsonMapper getChannel() throws ClientProtocolException, IOException, JSONException {
		DefaultHttpClient client = new DefaultHttpClient();
		TokenManager tokenManger = new TokenManager();		
		HttpGet request = new HttpGet(CHECK_CHANNEL_URL+"?channelUrl="+mChannelUrl+"&episodeUrl="+mLatestEpisodeUrl);
		tokenManger.putTokenInRequest(mPrefs, request);
		HttpResponse response;
		response = client.execute(request);
		if(response.getStatusLine().getStatusCode() == 200) {
			HttpResponseViewModel objResponse = new HttpResponseViewModel(response);
			JSONObject responseJson = new JSONObject(objResponse.getResponseBody());
			FeedChannelJsonMapper result = new FeedChannelJsonMapper(responseJson);
			return result;
		}
		
		else if(response.getStatusLine().getStatusCode() == 304) {
			return null;
		}
		
		else throw new IOException();
	}

	
	



}
