package com.toastworks.podimio.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.toastworks.podimio.core.RevisionManager;
import com.toastworks.podimio.core.TokenManager;
import com.toastworks.podimio.model.FeedChannelJsonMapper;
import com.toastworks.podimio.model.HttpResponseViewModel;
import com.toastworks.podimio.persistence.PodcastRepository;

public class SyncService extends IntentService {
	private static final String GET_CHANNELS_URL = "https://podcastapi.azurewebsites.net/breeze/subscriptions/all";
	private static final String GET_CHANNEL_URL = "https://podcastapi.azurewebsites.net/breeze/breeze/channel";
	PodcastRepository mDb;
	SharedPreferences mPrefs;
	List<String> mCurrentChannels = new ArrayList<String>();
	List<String> mUpdatedChannels = new ArrayList<String>();
	List<String> mChannelsToDelete = new ArrayList<String>();
	List<String> mChannelsToAdd = new ArrayList<String>();
	String mRevisionId;

	public static final String NOTIFICATION = "com.toastworks.podimio.receiver";
	
	public SyncService() {
		super("SyncService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		RevisionManager revManager = new RevisionManager();
		mRevisionId = intent.getStringExtra("revisionId");
		Context context = getApplicationContext();
		mPrefs = context.getSharedPreferences("com.toastworks.podimo",
				Context.MODE_PRIVATE);
		mDb = new PodcastRepository(getBaseContext());
		mCurrentChannels = mDb.getAllChannelUrls();
		try {
			mUpdatedChannels = getUpdatedChannels();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getChannelsToDelete();
		getChannelsToAdd();
		
		if(mChannelsToDelete.size() > 0) deleteChannels();
		if(mChannelsToAdd.size() > 0) addChannels();
		
		
		revManager.updateRevision(mPrefs, mRevisionId);
		publishResults();
	}



	

	private void publishResults() {
		  Intent intent = new Intent(NOTIFICATION);
		    intent.putExtra("Success", "Subscriptions Synced!");
		    sendBroadcast(intent);	
	}

	private void getChannelsToAdd() {
		mChannelsToAdd.addAll(mUpdatedChannels);
		mChannelsToAdd.removeAll(mCurrentChannels);
		
	}

	private void getChannelsToDelete() {
		mChannelsToDelete.addAll(mCurrentChannels);
		mChannelsToDelete.removeAll(mUpdatedChannels);
	}
	
	private void deleteChannels() {
		 for(String channelUrl : mChannelsToDelete) mDb.deleteChannel(channelUrl);
	}
	
	private void addChannels() {
		for(String channelUrl : mChannelsToAdd) addChannelToDb(channelUrl); 		
	}
	



	private void addChannelToDb(String channelUrl) {
		FeedChannelJsonMapper channel = null;

	    try {
			channel = getChannel(channelUrl);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mDb.insertChannel(channel);
	}

	private FeedChannelJsonMapper getChannel(String channelUrl) throws ClientProtocolException, IOException, JSONException {
		DefaultHttpClient client = new DefaultHttpClient();
		TokenManager tokenManger = new TokenManager();		
		HttpGet request = new HttpGet(GET_CHANNEL_URL+"?url="+channelUrl);
		tokenManger.putTokenInRequest(mPrefs, request);
		HttpResponse response;
		response = client.execute(request);
		HttpResponseViewModel objResponse = new HttpResponseViewModel(response);
		JSONObject responseJson = new JSONObject(objResponse.getResponseBody());
		FeedChannelJsonMapper result = new FeedChannelJsonMapper(responseJson);
		return result;
	}

	private List<String> getUpdatedChannels() throws JSONException, ClientProtocolException, IOException {
		List<String> updatedChannels = new ArrayList<String>();
		TokenManager tokenManger = new TokenManager();
		DefaultHttpClient client = new DefaultHttpClient();
		HttpResponse response;
		HttpGet request = new HttpGet(GET_CHANNELS_URL);
		tokenManger.putTokenInRequest(mPrefs, request);
		
        response = client.execute(request);
		
		HttpResponseViewModel objResponse = new HttpResponseViewModel(response);		
		
        JSONArray urls = new JSONArray(objResponse.getResponseBody());
        
        
        for(int i = 0; i < urls.length(); i++){
        	JSONObject item = urls.getJSONObject(i);
        	updatedChannels.add(item.getString("ChannelUrl"));
        }
		
		return updatedChannels;
	}
	



}
