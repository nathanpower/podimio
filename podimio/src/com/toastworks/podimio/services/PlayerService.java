package com.toastworks.podimio.services;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.Toast;

import com.toastworks.podimio.PlayerActivity;
import com.toastworks.podimio.core.PlayListManager;
import com.toastworks.podimio.interfaces.PlayerUiUpdateListener;
import com.toastworks.podimio.interfaces.PlayerUpdateListener;
import com.toastworks.podimio.model.PlayListState;
import com.toastworks.podimio.persistence.PodcastRepository;
import com.toastworks.podimo.R;
import com.toastworks.utilities.Utilities;

public class PlayerService extends Service implements OnCompletionListener, 
SeekBar.OnSeekBarChangeListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener {

	private IntentFilter mIntentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
	private NoisyAudioStreamReceiver noisyAudioStreamReciever = new NoisyAudioStreamReceiver();
	public static boolean isRunning = false;
	private PlayListManager mPlayListManager = new PlayListManager();
	private ArrayList<String> mEpisodesList = new ArrayList<String>();
	private String notificationText = "Playing...";
	private SharedPreferences mPrefs;
	public MediaPlayer mp = null;
	private Utilities mUtils;
	private PlayerUiUpdateListener mPlayerUI;
	private PlayerUpdateListener mPlaylistUI;
	private int currentEpisodeIndex = 0; 
	private String currentEpisodeUrl = "";
	private boolean mIsSingleEpisodePlay = false;
	
	public String getCurrentEpisode() {
		return mEpisodesList.get(currentEpisodeIndex);
	}
	
    private NotificationManager mNM;

    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = 12345;

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        public PlayerService getService() {
            return PlayerService.this;
        }
    }
    
    
    public void connectUIActivity(PlayerUiUpdateListener playerUI){
    	mPlayerUI = playerUI;
    }
    
    public void disconnectUIActivity(){
    	mPlayerUI = null;
    }
    
    public void connectPlaylistUIActivity(PlayerUpdateListener playListUI){
    	mPlaylistUI = playListUI;
    	if(mp.isPlaying()){
    		mPlaylistUI.setCurrentEpisodeUrl(currentEpisodeUrl);
    	}
    }
    
    public void disconnectPlaylistUIActivity(){
    	mPlaylistUI = null;
    }
    
    
    

    @Override
    public void onCreate() {
    	mp = new MediaPlayer();
    	mp.setOnPreparedListener(this);
    	mp.setOnCompletionListener(this);
    	mPrefs = this.getSharedPreferences(
			      "com.toastworks.podimo", Context.MODE_PRIVATE);
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        isRunning = true;
        mUtils = new Utilities();	
        mPlayListManager.deleteCurrentlyPlayingUrl(mPrefs);
        // Display a notification about us starting.  We put an icon in the status bar.
        showNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	if(intent != null){
	    	Bundle extras = intent.getExtras();
	        if (extras != null) {
	        	if(extras.getStringArrayList("episodesList") != null){
	        		mEpisodesList = extras.getStringArrayList("episodesList");
	        		if(mEpisodesList != null & mEpisodesList.size() == 1) mIsSingleEpisodePlay = true;
	        		else if(mEpisodesList != null & mEpisodesList.size() > 1) mIsSingleEpisodePlay = false;
	        	}
			}
    	}
        if(mEpisodesList.size() == 1) notificationText = "Currently playing single episode.";
        else notificationText = "Currently playing playlist.";
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    	 if(mPlayerUI != null){
 			mPlayerUI.stopProgressBar();
 			}
 		if(mPlaylistUI != null){
 			mPlaylistUI.playerIsStopped((Activity) mPlayerUI);
 		}
 		
 		if(mp != null && mp.isPlaying()){
 			mp.stop();
 			mp = null;
 		}
        // Cancel the persistent notification.
        mNM.cancel(NOTIFICATION);
        isRunning = false;
        mPlayListManager.deleteCurrentlyPlayingUrl(mPrefs);
       
        // Tell the user we stopped.
        Toast.makeText(this, "Player Service Stopped", Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {
    	Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.img_btn_play_large_icon);

        Intent notificationIntent = new Intent(this, PlayerActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Notification notification = new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.img_btn_play_icon)
        .setLargeIcon(largeIcon)
        .setTicker(notificationText)
        .setWhen(System.currentTimeMillis())
        .setContentTitle("Podimio Player")
        .setContentText(notificationText)
        .setContentIntent(PendingIntent.getActivity(this, 0,
                notificationIntent, 0))
        .setAutoCancel(true).build();       

        mNM.notify(NOTIFICATION, notification);
        startForeground(NOTIFICATION, notification);
    }

	
	public void playEpisode(int episodeIndex){
		try {
			if(mPlayerUI != null){
				mPlayerUI.showStreamProgressBar();
				}
        	mp.reset();
        	mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        	mp.setDataSource(mEpisodesList.get(episodeIndex));
			mp.prepareAsync();
			
			
		} catch (IllegalArgumentException e) {
			mPlayerUI.makeToast("Something went wrong while attempting to stream episode");
			e.printStackTrace();
		} catch (IllegalStateException e) {
			mPlayerUI.makeToast("Something went wrong while attempting to stream episode");
			e.printStackTrace();
		} catch (IOException e) {
			mPlayerUI.makeToast("Something went wrong while attempting to stream episode");
			e.printStackTrace();
		} catch (Exception e){
			mPlayerUI.makeToast("Something went wrong while attempting to stream episode");
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		
	}

	
	/**
	 * When user starts moving the progress handler
	 * */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		if(mPlayerUI != null)
			mPlayerUI.stopProgressBar();
		
	}

	
	/**
	 * When user stops moving the progress handler
	 * */

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		if(mPlayerUI != null)
			mPlayerUI.stopProgressBar();
		int totalDuration = mp.getDuration();
		int currentPosition = mUtils.progressToTimer(seekBar.getProgress(), totalDuration);
		
		// forward or backward to certain seconds
		mp.seekTo(currentPosition);
		
		// update timer progress again
		if(mPlayerUI != null)
			mPlayerUI.startProgressBar();
	}

	
	@Override
	public void onCompletion(MediaPlayer arg0) {
		// unRegisterNoisyAudioReciever();
		mPlayListManager.deleteCurrentlyPlayingUrl(mPrefs);	
		if(mPlayerUI != null){
			mPlayerUI.stopProgressBar();
			}
		if(mPlaylistUI != null){
			mPlaylistUI.playerIsStopped((Activity) mPlayerUI);
		}
		
		if(mIsSingleEpisodePlay) stopSelf();
		
		updatePlaylistStateIfChanged();
		
		//  play next episode
		if(currentEpisodeIndex < (mEpisodesList.size() - 1)){
			playEpisode(currentEpisodeIndex + 1);
			currentEpisodeIndex = currentEpisodeIndex + 1;
		}else{
			// finished playlist, stop service
			stopSelf();
		}
	}

	private void updatePlaylistStateIfChanged() {
		PlayListState playListState = new PlayListState(mEpisodesList, currentEpisodeIndex);
		playListState = mPlayListManager.getRevisedPlayListState(playListState, new PodcastRepository(getApplicationContext()));
		
		mEpisodesList = playListState.getEpisodeList();
		currentEpisodeIndex = playListState.getCurrentEpisode();
	}
	
	public void nextEpisode(){
		if(mPlayerUI != null)
			mPlayerUI.stopProgressBar();
		updatePlaylistStateIfChanged();
		
		// if user clicks next episode on single episode play, 
		// assume they want to play playlist
		if(mIsSingleEpisodePlay) currentEpisodeIndex = -1;
		
		// check if next episode is there or not
		if(currentEpisodeIndex < (mEpisodesList.size() - 1) && mEpisodesList.size() != 0){				
			playEpisode(currentEpisodeIndex + 1);
			currentEpisodeIndex = currentEpisodeIndex + 1;
		}else{
			// play first song
			playEpisode(0);
			currentEpisodeIndex = 0;
		}
	}
	
	public void previousEpisode() {
		if(mPlayerUI != null)
			mPlayerUI.stopProgressBar();
		updatePlaylistStateIfChanged();
		if(currentEpisodeIndex > 0){
			playEpisode(currentEpisodeIndex - 1);
			currentEpisodeIndex = currentEpisodeIndex - 1;
		}else{
			// play last song
			playEpisode(mEpisodesList.size() - 1);
			currentEpisodeIndex = mEpisodesList.size() - 1;
		}
	}
		
	

	@Override
	public void onPrepared(MediaPlayer arg0) {
		mp.start();
		registerReceiver(noisyAudioStreamReciever, mIntentFilter);
		currentEpisodeUrl = getCurrentEpisode();
		mPlayListManager.storeCurrentlyPlayingUrl(mPrefs, currentEpisodeUrl);
		if(mPlayerUI != null){
			mPlayerUI.hideStreamProgressBar();
			mPlayerUI.startProgressBar();
			mPlayerUI.displayEpisodeTitle(currentEpisodeUrl);
		}
		if(mPlaylistUI != null){
			mPlaylistUI.playerIsNowPlayingUrl(currentEpisodeUrl, (Activity) mPlayerUI);
		}
	}

@Override
public boolean onError(MediaPlayer arg0, int arg1, int arg2) {
	// unRegisterNoisyAudioReciever();
	if(mPlayerUI != null)
		mPlayerUI.makeToast("Something went wrong while attempting to stream episode");
	return false;
}

private class NoisyAudioStreamReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
            PlayerService.this.mp.pause();
            if(mPlayerUI != null) mPlayerUI.changeStateToPaused();          
        }
    }
}

public void unRegisterNoisyAudioReciever(){
	unregisterReceiver(noisyAudioStreamReciever);
}

	

}
