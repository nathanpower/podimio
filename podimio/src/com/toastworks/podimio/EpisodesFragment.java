package com.toastworks.podimio;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.toastworks.podimio.interfaces.EpisodeManager;
import com.toastworks.podimio.interfaces.EpisodeSelectedListener;
import com.toastworks.podimio.interfaces.ListViewSelectionHighlighter;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimio.model.FeedItemDao;
import com.toastworks.podimio.model.PlaylistItemViewModel;
import com.toastworks.podimio.persistence.PodcastRepository;
import com.toastworks.podimio.services.RefreshFeedService;
import com.toastworks.podimio.services.SyncService;
import com.toastworks.podimo.R;

public class EpisodesFragment extends ListFragment implements EpisodeManager{
		
	private ArrayAdapter<FeedItemDao> mAdapter;
	private List<FeedItemDao> mItemsList = new ArrayList<FeedItemDao>();
	private String mFeedUrl;
	private String mItemUrl;
	private String mFeedTitle;
	private String mFeedUrlKey;
	private String mItemUrlKey;
	private String mListKey;
	private String mFeedTitleKey;
	private Activity mParentActivity;
	private ToastMaker mToastMaker;
	private LayoutInflater  mInflater;
	private EpisodeSelectedListener mListener;
	private PodcastRepository mRepository;
	private BroadcastReceiver receiver = new BroadcastReceiver() {

	    @Override
	    public void onReceive(Context context, Intent intent) {
	      Bundle bundle = intent.getExtras();
			int duration = Toast.LENGTH_LONG;
			Toast toast = Toast.makeText(context, (CharSequence) bundle.get("Result"), duration);
			toast.show();
			mItemsList.clear();
			mItemsList.addAll(mRepository.getAllItemsForFeed(mFeedUrl));
			mAdapter.notifyDataSetChanged();
	    }
	  };
	
	  @Override
		public void onResume() {
		    super.onResume();
		    getActivity().registerReceiver(receiver, new IntentFilter(RefreshFeedService.NOTIFICATION));
		  }
		  @Override
		public void onPause() {
		    super.onPause();
		    getActivity().unregisterReceiver(receiver);
		  }
	  
	  @Override
	    public void onAttach(Activity activity) {
	      super.onAttach(activity);
	      if (activity instanceof EpisodeSelectedListener) {
	        mListener = (EpisodeSelectedListener) activity;
	        mToastMaker = (ToastMaker) activity;
	      } else {
	        throw new ClassCastException(activity.toString()
	            + " must implement ItemSelectedListener.OnItemSelected");
	      }
	    }
	
	@Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	      Bundle savedInstanceState) {
	    View view = inflater.inflate(R.layout.fragment_episodes,
	        container, false);
	    
		mInflater = inflater;

    
	    return view;
	  }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mRepository = new PodcastRepository(getActivity());
	    mParentActivity = getActivity();
	    mListKey = mParentActivity.getBaseContext().getString(R.string.items_list_key);
		mFeedUrlKey = mParentActivity.getBaseContext().getString(R.string.feed_url_key);
		mItemUrlKey = mParentActivity.getBaseContext().getString(R.string.item_url_key);
		mFeedTitleKey = mParentActivity.getBaseContext().getString(R.string.feed_title_key);

			
		if(savedInstanceState != null){
			mFeedUrl = savedInstanceState.getString(mFeedUrlKey);
			mItemUrl = savedInstanceState.getString(mItemUrlKey);
			mFeedTitle = savedInstanceState.getString(mFeedTitleKey);
			mItemsList = savedInstanceState.getParcelableArrayList(mListKey);		
		}
		else {
			Bundle extras = getActivity().getIntent().getExtras();
			if (extras != null) {
			    mFeedUrl = extras.getString(mFeedUrlKey);
			    mFeedTitle = extras.getString(mFeedTitleKey);
			    mItemsList = getItemsFromDb(mFeedUrl);
			}		
		}		

		getActivity().setTitle(mFeedTitle);
		
		 mAdapter = new ArrayAdapter<FeedItemDao>(  
	   		     mInflater.getContext(), R.layout.podcast_list_item,  
	   		     mItemsList);  
	   		   setListAdapter(mAdapter); 

		setListAdapter(mAdapter);

		ListView listView = getListView();
		registerForContextMenu(listView);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                int position, long id) {
            	FeedItemDao item = (FeedItemDao) parent.getItemAtPosition(position);        	
            	mListener.onItemSelected(item);
          	  
            }});

	}

	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putParcelableArrayList(mListKey, (ArrayList<? extends Parcelable>) mItemsList);
		outState.putString(mFeedUrlKey, mFeedUrl);
		outState.putString(mItemUrlKey, "");
		outState.putString(mFeedTitleKey, mFeedTitle);
		
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		getActivity().getMenuInflater().inflate(R.menu.episode_item_menu, menu);
	}

	

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.play_episode:
			  startPlayer(item);
			return true;
		case R.id.add_to_playlist:
				addEpisodeToPlaylist(item);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void addEpisodeToPlaylist(MenuItem item) {
		
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		 int episodeIndex = (int) info.id;
		 PlaylistItemViewModel selectedEpisode = 
				 new PlaylistItemViewModel(mItemsList.get(episodeIndex), mFeedTitle);
		 addEpisodeToPlayListDbTable(selectedEpisode);
	}

	private void addEpisodeToPlayListDbTable(
			PlaylistItemViewModel selectedEpisode) {
		mRepository.insertPlayListItem(selectedEpisode);	
		mToastMaker.makeToast("Episode added to Playlist");
	}


	private List<FeedItemDao> getItemsFromDb(String feedUrl) {
		PodcastRepository db = new PodcastRepository(mParentActivity.getBaseContext());
		return db.getAllItemsForFeed(feedUrl);
	}
	
	// TODO: Repeated code here... sort it out!!!

	private void startPlayer(MenuItem item) {
		ArrayList<String> episodesList = new ArrayList<String>();
		 AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		 int episodeIndex = (int) info.id;
		 mItemUrl = mItemsList.get(episodeIndex).getUrl();
		 episodesList.add(mItemUrl);
		 Intent intent = new Intent(getActivity().getBaseContext(), PlayerActivity.class);
	   	 //intent.putExtra(mItemUrlKey, mItemUrl);
		 intent.putStringArrayListExtra("episodesList", episodesList);
	   	 startActivity(intent);
	}

	@Override
	public void startPlayerForEpisode(FeedItemDao item) {
		ArrayList<String> episodesList = new ArrayList<String>();
		episodesList.add(item.getUrl());
		Intent intent = new Intent(getActivity().getBaseContext(), PlayerActivity.class);
	   	 //intent.putExtra(mItemUrlKey, mItemUrl);
		 intent.putStringArrayListExtra("episodesList", episodesList);
	   	 startActivity(intent);		
	   	 getActivity().finish();
	}

	@Override
	public void addEpisodeToPlayList(FeedItemDao episode) {
		PlaylistItemViewModel selectedEpisode = 
				 new PlaylistItemViewModel(episode, mFeedTitle);
		addEpisodeToPlayListDbTable(selectedEpisode);		
	}

	@Override
	public void checkForNewEpisodes() {
		 mToastMaker.makeToast("Checking for new episodes");
		 Intent intent = new Intent(getActivity(), RefreshFeedService.class);
		 intent.putExtra("channelUrl", mFeedUrl);
		 intent.putExtra("episodeUrl", mItemsList.get(0).getUrl());
		    getActivity().startService(intent);
	}



	

	
		
}
