package com.toastworks.podimio;

import java.util.ArrayList;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.toastworks.podimio.core.PlayListManager;
import com.toastworks.podimio.interfaces.PlayerUiUpdateListener;
import com.toastworks.podimio.model.PlaylistItemViewModel;
import com.toastworks.podimio.persistence.PodcastRepository;
import com.toastworks.podimio.services.PlayerService;
import com.toastworks.podimo.R;
import com.toastworks.utilities.Utilities;

public class PlayerActivity extends Activity implements 
 PlayerUiUpdateListener{
	
	private PlayerService mBoundService;
	private boolean mIsBound;
	
	private ImageButton btnPlay;
	private ImageButton btnForward;
	private ImageButton btnBackward;
	private ImageButton btnNext;
	private ImageButton btnPrevious;
	private ImageButton btnPlaylist;
	private SeekBar episodeProgressBar;
	private TextView songTitleLabel;
	private TextView songCurrentDurationLabel;
	private TextView songTotalDurationLabel;
	private Handler mHandler = new Handler();;
	private Utilities utils;
	private int seekForwardTime = 5000; // 5000 milliseconds
	private int seekBackwardTime = 5000; // 5000 milliseconds
	private ArrayList<String> mEpisodesList = new ArrayList<String>();
	private PlaylistItemViewModel mCurrentEpisode;
	private PodcastRepository mRepository;
	private PlayListManager mPlaylistManager;
	private SharedPreferences mPrefs;
	private boolean mPlayNow = false;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.player);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mRepository = new PodcastRepository(this);
		mPlaylistManager = new PlayListManager();
		mPrefs = this.getSharedPreferences(
			      "com.toastworks.podimo", Context.MODE_PRIVATE);
		mPlayNow = false;
		setUpButtons();
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.getStringArrayList("episodesList") != null) { // Activity started with play command
		    mEpisodesList = extras.getStringArrayList("episodesList");
		    getIntent().removeExtra("episodesList");
		    if(mEpisodesList.size() > 0) {
		    	mPlayNow = true;
		    	startPlayerService();
			    mPlaylistManager.storePlaybackStateImage(mPrefs, Integer.toString(R.drawable.btn_play));
		    }
		    
		}
		else if(mIsBound == false){// Activity was started from notification bar
			doBindService();
		}
		else if(mIsBound == true){ 
			setButtonsAndSeekbarState();	
		}

		utils = new Utilities();	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.player_activity_actions, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.kill_player:
	            killPlayer();
	            return true;
	        case R.id.open_playlist:
	            goToPlayList();
	            return true;
	         // Respond to the action bar's Up/Home button
	        case android.R.id.home:
	            NavUtils.navigateUpFromSameTask(this);
	            return true;	        
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void killPlayer() {
		if(mIsBound){
			stopProgressBar();
			mBoundService.stopSelf();
			finish();
		}
	}

	private void startPlayerService() {
		Intent intent = new Intent(PlayerActivity.this, 
	            PlayerService.class);
		intent.putStringArrayListExtra("episodesList", mEpisodesList);
		startService(intent);
	    doBindService();
	    mIsBound = true;
		
	}

	private ServiceConnection mConnection = new ServiceConnection() {
	    public void onServiceConnected(ComponentName className, IBinder service) {
	        // This is called when the connection with the service has been
	        // established, giving us the service object we can use to
	        // interact with the service.  Because we have bound to a explicit
	        // service that we know is running in our own process, we can
	        // cast its IBinder to a concrete class and directly access it.
	        PlayerActivity.this.mBoundService = ((PlayerService.LocalBinder)service).getService();
	        PlayerActivity.this.setUpListeners();			
	        PlayerActivity.this.mBoundService.connectUIActivity(PlayerActivity.this);
	        PlayerActivity.this.episodeProgressBar.setOnSeekBarChangeListener(mBoundService); // Important
	        PlayerActivity.this.makeToast("Player UI Connected to Player Service");
	        
	        if(PlayerActivity.this.mPlayNow) {
	        	PlayerActivity.this.getStreamProgressBar().setVisibility(View.VISIBLE);
	        	PlayerActivity.this.playEpisode(0);
	        }
	        else {
	        	setButtonsAndSeekbarState();				
	        }
	    }


	    public void onServiceDisconnected(ComponentName className) {
	        // This is called when the connection with the service has been
	        // unexpectedly disconnected -- that is, its process crashed.
	        // Because it is running in our same process, we should never
	        // see this happen.
	    	PlayerActivity.this.mBoundService = null;
	    	PlayerActivity.this.makeToast("Player UI Disconnected from Player Service");
	    }
	};
	

	void doBindService() {
	    // Establish a connection with the service.  We use an explicit
	    // class name because we want a specific service implementation that
	    // we know will be running in our own process (and thus won't be
	    // supporting component replacement by other applications).
		bindService(new Intent(PlayerActivity.this, 
	            PlayerService.class), mConnection, Context.BIND_ABOVE_CLIENT);
	    mIsBound = true;
	}

	void doUnbindService() {
	    if (mIsBound) {
	        // Detach our existing connection.
	        unbindService(mConnection);
	        mIsBound = false;
	    }
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    doUnbindService();
	}
	
	

	private void setUpListeners() {
				
		/**
		 * Play button click event
		 * */
		btnPlay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// check for already playing
				if(mBoundService.mp.isPlaying()){
					if(mBoundService.mp != null){
						mBoundService.mp.pause();
						// Changing button image to play button
						PlayerActivity.this.changeStateToPaused();
					}
				}else{
					// Resume song
					if(mBoundService.mp!=null){
						mBoundService.mp.start();
						// Changing button image to pause button
						PlayerActivity.this.changeStateToPlaying();
					}
				}
				
			}

			
		});
		
		/**
		 * Forward button click event
		 * */
		btnForward.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// get current song position				
				int currentPosition = mBoundService.mp.getCurrentPosition();
				// check if seekForward time is lesser than song duration
				if(currentPosition + seekForwardTime <= mBoundService.mp.getDuration()){
					// forward song
					mBoundService.mp.seekTo(currentPosition + seekForwardTime);
				}else{
					// forward to end position
					mBoundService.mp.seekTo(mBoundService.mp.getDuration());
				}
			}
		});
		
		/**
		 * Back button click event
		 * */
		btnBackward.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// get current song position				
				int currentPosition = mBoundService.mp.getCurrentPosition();
				// check if seekBackward time is greater than 0 sec
				if(currentPosition - seekBackwardTime >= 0){
					// forward song
					mBoundService.mp.seekTo(currentPosition - seekBackwardTime);
				}else{
					// backward to starting position
					mBoundService.mp.seekTo(0);
				}
				
			}
		});
		
		/**
		 * Next button click event
		 * */
		btnNext.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mBoundService.nextEpisode();				
			}
		});
		
		/**
		 * Back button click event
		 * */
		btnPrevious.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mBoundService.previousEpisode();			
			}
		});
		

		/**
		 * PLaylist button click event
		 * */
	/*	btnPlaylist.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				goToPlayList();
			}
	
		});*/
	}

	private void setUpButtons() {
		// All player buttons
		btnPlay = (ImageButton) findViewById(R.id.btnPlay);
		btnForward = (ImageButton) findViewById(R.id.btnForward);
		btnBackward = (ImageButton) findViewById(R.id.btnBackward);
		btnNext = (ImageButton) findViewById(R.id.btnNext);
		btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
		// btnPlaylist = (ImageButton) findViewById(R.id.btnPlaylist);
		episodeProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
		songTitleLabel = (TextView) findViewById(R.id.songTitle);
		songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
		songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
	}
	


	public void playEpisode(int episodeIndex){

			// displayEpisodeTitle(Index);
		
			mBoundService.playEpisode(episodeIndex);
			
        	// Changing Button Image to pause image
			btnPlay.setImageResource(R.drawable.btn_pause);
			
			// set Progress bar values
			episodeProgressBar.setProgress(0);
			episodeProgressBar.setMax(100);
			
	}

	@Override
	public void displayEpisodeTitle(String episodeUrl) {
		mCurrentEpisode = mRepository.getEpisodeForUrl(episodeUrl);
		songTitleLabel.setText(mCurrentEpisode.getFeedTitle() + " - " + mCurrentEpisode.getEpisodeTitle());
	}
	
	/**
	 * Background Runnable thread
	 * */
	private Runnable mUpdateTimeTask = new Runnable() {
		   public void run() {
			   try {
				   if(mBoundService.mp.isPlaying()){
					   long totalDuration = mBoundService.mp.getDuration();
					   long currentPosition = mBoundService.mp.getCurrentPosition();
					  
					   // Displaying Total Duration time
					   songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
					   // Displaying time completed playing
					   songCurrentDurationLabel.setText(""+utils.milliSecondsToTimer(currentPosition));
					   
					   // Updating progress bar
					   int progress = (int)(utils.getProgressPercentage(currentPosition, totalDuration));
					   //Log.d("Progress", ""+progress);
					   episodeProgressBar.setProgress(progress);
				   }
			   }
			   catch(NullPointerException npe){
				   Log.e("Player Seekbar Error", "Update Time Task Could not acces Player Service");
			   }
			   
			   // Run this thread after 100 milliseconds
		       mHandler.postDelayed(this, 100);
		   }
		};
		


	
	@Override
	public void stopProgressBar() {
		// remove message Handler from updating progress bar
		mHandler.removeCallbacks(mUpdateTimeTask);
    }
	
	

	@Override
	public void startProgressBar() {
		 mHandler.postDelayed(mUpdateTimeTask, 100);   
	}



	@Override
	public void makeToast(String message) {
		Toast.makeText(PlayerActivity.this, message,
                Toast.LENGTH_SHORT).show();
	}
	
	private ProgressBar getStreamProgressBar() {
		return (ProgressBar) findViewById(R.id.streamProgressBar);
	}
	

	@Override
	public void hideStreamProgressBar() {
		getStreamProgressBar().setVisibility(View.GONE);
		
	}

	@Override
	public void showStreamProgressBar() {
		getStreamProgressBar().setVisibility(View.VISIBLE);
		
	}
	
	
	private void changeStateToPlaying() {
		btnPlay.setImageResource(R.drawable.btn_pause);
		mPlaylistManager.storePlaybackStateImage(mPrefs, Integer.toString(R.drawable.btn_play));
	}

	@Override
	public void changeStateToPaused() {
		btnPlay.setImageResource(R.drawable.btn_play);
		mPlaylistManager.storePlaybackStateImage(mPrefs, Integer.toString(R.drawable.btn_pause));
	}

	private void setButtonsAndSeekbarState() {
		startProgressBar();
		displayEpisodeTitle(mBoundService.getCurrentEpisode());
		if(mBoundService.mp.isPlaying()) PlayerActivity.this.changeStateToPlaying();
		else changeStateToPaused();
	}
	
	private void goToPlayList() {
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.putExtra("GoToPlayList", true);	
		if(mIsBound && mBoundService.mp.isPlaying()) 
			intent.putExtra("CurrentEpisodeUrl", mBoundService.getCurrentEpisode());
		startActivity(intent);
		finish();
	}





	
}