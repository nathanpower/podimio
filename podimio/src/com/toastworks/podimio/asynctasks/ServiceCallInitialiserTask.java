package com.toastworks.podimio.asynctasks;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.toastworks.podimio.core.TokenManager;
import com.toastworks.podimio.interfaces.AsyncSvcCallInitListener;
import com.toastworks.podimio.model.RequestMethod;
import com.toastworks.podimio.model.ServiceRequest;
 
public class ServiceCallInitialiserTask extends AsyncTask<ServiceRequest, Integer, HttpRequestBase>{
 
	private AsyncSvcCallInitListener mListener;
    private SharedPreferences mPrefs;
    

    public ServiceCallInitialiserTask(AsyncSvcCallInitListener listener, SharedPreferences prefs){
    	mPrefs = prefs;
    	mListener = listener;
    	
    }
    
	@Override
	protected HttpRequestBase doInBackground(ServiceRequest... requestInfo) {
		RequestMethod method = requestInfo[0].getMethod();
		String url = requestInfo[0].getUrl();
		List<NameValuePair> params = requestInfo[0].getParams();
        TokenManager tokenManger = new TokenManager();
         
        if (method == RequestMethod.POST) {
            HttpPost httpPost = new HttpPost(url);
            tokenManger.putTokenInRequest(mPrefs, httpPost);
            
            if (params != null) {
                try {
					httpPost.setEntity(new UrlEncodedFormEntity(params));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
            }
            
            return httpPost;


        } else if (method == RequestMethod.GET) {
        	
            if (params != null) {
                String paramString = URLEncodedUtils
                        .format(params, "utf-8");
                url += "?" + paramString;
            }
            HttpGet httpGet = new HttpGet(url);
            tokenManger.putTokenInRequest(mPrefs, httpGet);
            
            return httpGet;
        }      
        
    return null;
	}

    @Override
	protected void onPostExecute(HttpRequestBase result) {
		super.onPostExecute(result);
		mListener.onTaskCompleted(result);
	}

}