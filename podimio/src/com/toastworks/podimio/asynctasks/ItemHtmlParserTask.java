package com.toastworks.podimio.asynctasks;

import android.os.AsyncTask;
import android.text.Html;

import com.toastworks.podimio.interfaces.ItemsHtmlViewer;

public class ItemHtmlParserTask extends
		AsyncTask<String, Integer, CharSequence> {

	ItemsHtmlViewer mListener;
	
	public ItemHtmlParserTask(ItemsHtmlViewer listener){
		mListener = listener;
	}
	
	@Override
	protected CharSequence doInBackground(
			String... items) 
			{
		return Html.fromHtml(items[0]);
	}

	@Override
	protected void onPostExecute(CharSequence result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		mListener.onItemHtmlParsed(result);
	}
	
	

}
