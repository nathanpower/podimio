package com.toastworks.podimio.asynctasks;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.toastworks.podimio.interfaces.AsyncChannelParseTaskListener;
import com.toastworks.podimio.model.FeedChannelJsonMapper;

public class ChannelJsonParserTask extends AsyncTask<String, Integer, FeedChannelJsonMapper>

{
	private ProgressBar mProgressBar;
	private AsyncChannelParseTaskListener mListener;
	
	public ChannelJsonParserTask(AsyncChannelParseTaskListener listener, ProgressBar progressBar){
		mProgressBar = progressBar;
		mListener = listener;
	}
	
	@Override
	protected FeedChannelJsonMapper doInBackground(String... params) {
		FeedChannelJsonMapper channel = null;	

	    try {
			channel = new FeedChannelJsonMapper(new JSONObject(params[0]));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return channel;
	}

	@Override
	protected void onPostExecute(FeedChannelJsonMapper channel) {
		// TODO Auto-generated method stub
		super.onPostExecute(channel);
		if(mProgressBar != null)
			mProgressBar.setVisibility(View.GONE);
		mListener.onTaskCompleted(channel);
		
	}
	
	





	
}
