package com.toastworks.podimio.asynctasks;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.toastworks.podimio.interfaces.AsyncServiceTaskListener;
import com.toastworks.podimio.model.HttpResponseViewModel;

public class ServiceHandlerTask extends AsyncTask<HttpRequestBase, Integer, HttpResponseViewModel>{

	private ProgressBar mProgressBar;
	private AsyncServiceTaskListener mListener;
	
	public ServiceHandlerTask(AsyncServiceTaskListener listener, ProgressBar progressBar){
		mProgressBar = progressBar;
		mListener = listener;
	}
	
	@Override
	protected HttpResponseViewModel doInBackground(HttpRequestBase... request) {
		
        HttpResponse httpResponse = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();

		try{
			if(request[0] instanceof HttpGet)
				httpResponse = httpClient.execute((HttpGet) request[0]);
			else if(request[0] instanceof HttpPost)
				httpResponse = httpClient.execute((HttpPost) request[0]);
			else return null;
			
			
		} catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            
        }
		HttpResponseViewModel objResponse = new HttpResponseViewModel(httpResponse);
		return objResponse;
	}
	
	
	@Override
	protected void onPostExecute(HttpResponseViewModel response) {
		super.onPostExecute(response);
		if(mProgressBar != null)
			mProgressBar.setVisibility(View.GONE);
		mListener.onTaskCompleted(response);
	}
	

}
