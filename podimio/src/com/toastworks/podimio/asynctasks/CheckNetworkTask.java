package com.toastworks.podimio.asynctasks;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

import com.toastworks.podimio.interfaces.NetworkCheckCompleteListener;

public class CheckNetworkTask extends AsyncTask<String, Void, Boolean> {
    NetworkCheckCompleteListener<Boolean> callback;

    public CheckNetworkTask(NetworkCheckCompleteListener<Boolean> callback) {
        this.callback = callback;
    }

    protected Boolean doInBackground(String... params) {
        int code = 0;
        try {
            URL u = new URL(params[0]);
            HttpURLConnection huc = (HttpURLConnection) u.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();
            code = huc.getResponseCode();
        } catch (IOException e) {
            return false;
        } catch (Exception e) {
            return false;
        }

        return code == 200;
    }

    protected void onPostExecute(Boolean result){
          callback.onNetworkCheckComplete(result);
    }
}
