package com.toastworks.podimio.asynctasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.AsyncTask;
import android.text.Html;

import com.toastworks.podimio.interfaces.ItemsHtmlViewer;
import com.toastworks.podimio.model.FeedItemDao;

public class ItemsHtmlParserTask extends
		AsyncTask<List<FeedItemDao>, Integer, List<Map<String, CharSequence>>> {

	ItemsHtmlViewer mListener;
	List<Map<String, CharSequence>> mItemsViewData = new ArrayList<Map<String, CharSequence>>();
	
	public ItemsHtmlParserTask(ItemsHtmlViewer listener){
		mListener = listener;
	}
	
	@Override
	protected List<Map<String, CharSequence>> doInBackground(
			List<FeedItemDao>... itemsList) 
			{
		for (FeedItemDao item : itemsList[0]) {
			Map<String, CharSequence> datum = new HashMap<String, CharSequence>(2);
			datum.put("title", item.getTitle());
			datum.put("description", Html.fromHtml(item.getDescription()));
			mItemsViewData.add(datum);
		}
		
		return mItemsViewData;
	}

	@Override
	protected void onPostExecute(List<Map<String, CharSequence>> result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		mListener.onItemsHtmlParsed(result);
	}
	
	

}
