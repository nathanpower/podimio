package com.toastworks.podimio;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.toastworks.podimio.asynctasks.ServiceCallInitialiserTask;
import com.toastworks.podimio.asynctasks.ServiceHandlerTask;
import com.toastworks.podimio.core.PlayListManager;
import com.toastworks.podimio.core.RevisionManager;
import com.toastworks.podimio.core.TokenManager;
import com.toastworks.podimio.interfaces.AsyncServiceTaskListener;
import com.toastworks.podimio.interfaces.AsyncSvcCallInitListener;
import com.toastworks.podimio.interfaces.ListViewUpdateListener;
import com.toastworks.podimio.interfaces.NetworkChecker;
import com.toastworks.podimio.interfaces.PlayerStopper;
import com.toastworks.podimio.interfaces.PlayerUpdateListener;
import com.toastworks.podimio.interfaces.SubscriptionManager;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimio.model.FeedChannelJsonMapper;
import com.toastworks.podimio.model.HttpResponseViewModel;
import com.toastworks.podimio.model.RequestMethod;
import com.toastworks.podimio.model.ServiceRequest;
import com.toastworks.podimio.persistence.PodcastRepository;
import com.toastworks.podimio.services.PlayerService;
import com.toastworks.podimo.R;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener, SubscriptionManager, ToastMaker, 
		AsyncSvcCallInitListener, AsyncServiceTaskListener, NetworkChecker, 
		PlayerUpdateListener, PlayerStopper {

	SharedPreferences _prefs;
	private static final String CURRENT_EPISODE_KEY = "CurrentEpisodeKey";
	private boolean mSubscriptionsUpdated = false;
	public static final String NEW_SUBS_FLAG_KEY = "NewSubscriptionsAdded";
	public static final String SUBS_FRAG_KEY = "SubscriptionFragment";
	private ListViewUpdateListener mSubsUpdate;
	private PlayerUpdateListener mPlayList;
	private SubApplication mApp;
	private String mSubsFragId;
	private PlayerService mBoundService;
	private boolean mIsBound;
	private String mCurrentEpisodeUrl = "";
	private PlayListManager mPlaylistManager;
	
	public String getCurrentEpisodeUrl(){
		return mBoundService.getCurrentEpisode();
	}
	

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		;

		_prefs = this.getSharedPreferences("com.toastworks.podimo",
				Context.MODE_PRIVATE);
		mPlaylistManager = new PlayListManager();
		// Need a reference to subscription fragment as we are calling a method on it
		List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
		if(fragmentList != null && !fragmentList.isEmpty()){
			mSubsUpdate = (ListViewUpdateListener) fragmentList.get(0);
		}

		if (savedInstanceState == null)
			mSubscriptionsUpdated = false;
		else {
			mSubscriptionsUpdated = savedInstanceState
					.getBoolean(NEW_SUBS_FLAG_KEY);
			mCurrentEpisodeUrl = savedInstanceState
					.getString(CURRENT_EPISODE_KEY);
		}

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
		
		Bundle extras = getIntent().getExtras();
		
		if(extras != null){
			if(extras.getBoolean("GoToPlayList")) {
				mViewPager.setCurrentItem(2);
			}
			
		}
		
		if(!PlayerService.isRunning){
			mPlaylistManager.deleteCurrentlyPlayingUrl(_prefs);
		}
			
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(!mIsBound)
			doBindService();
		
		else{
			// updatePlaylistWithCurrentPlayingEpisode();
		}
		invalidateOptionsMenu();
	}


	@Override
	public void onPause() {
		Bundle bundle = new Bundle();
		bundle.putString(CURRENT_EPISODE_KEY, mCurrentEpisodeUrl);
		bundle.putBoolean(NEW_SUBS_FLAG_KEY, mSubscriptionsUpdated);
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options_menu, menu);
		if(mIsBound && mBoundService != null && mBoundService.mp != null && mBoundService.mp.isPlaying()){
			menu.findItem(R.id.open_player).setIcon(R.drawable.ic_media_play);
			menu.findItem(R.id.open_player).setVisible(true);
		}
		else if(mIsBound && mBoundService != null && mBoundService.mp != null){		
			menu.findItem(R.id.open_player).setIcon(R.drawable.ic_media_pause);
			menu.findItem(R.id.open_player).setVisible(true);
		}
		else menu.findItem(R.id.open_player).setVisible(false);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		TokenManager tokenManager = new TokenManager();
		Intent intent;
		switch (item.getItemId()) {
		case R.id.sign_out:
			mApp = (SubApplication ) getApplicationContext();
			mApp.hasRefreshed = false;
			tokenManager.deleteToken(_prefs);
			intent = new Intent(getApplicationContext(), AccountActivity.class);
			startActivity(intent);
			return true;
		case R.id.open_player:
			openPlayer();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = null;
			switch (position) {
			case 0:
				fragment = new SubscriptionsFragment();
				mSubsUpdate = (ListViewUpdateListener) fragment;
				break;
			case 1:
				fragment = new SearchFragment();
				break;
			case 2:
				fragment = new PlaylistFragment();
				mPlayList = (PlayerUpdateListener) fragment;
				break;
			}

			return fragment;

		}

		

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}
	

	@Override
	public void addSubscription(FeedChannelJsonMapper channel) {
		mSubscriptionsUpdated = true;
		Context context = getBaseContext();
		PodcastRepository db = new PodcastRepository(context);

		if (db.FeedRecordExists(channel.getUrl())) {
			makeToast("Feed already in subscriptions!");
		} else {
			saveChannelToDb(channel, db);
		}

	}
	
	@Override
	public void deleteSubscription(String url) {
		mSubscriptionsUpdated = true;
		Context context = getBaseContext();
		PodcastRepository db = new PodcastRepository(context);

		if (!db.FeedRecordExists(url)) {
			makeToast("Feed does not exist in local db!");
		} else {
			deleteChannelFromDb(url, db);
		}

	}

	private void deleteChannelFromDb(String url, PodcastRepository db) {
			deleteChannelFromSubscriptionsStore(url);
			db.deleteChannel(url);
			mSubsUpdate.updateListView(this);
			mSubscriptionsUpdated = false;
			makeToast("Subscription deleted");
	}





	private void saveChannelToDb(FeedChannelJsonMapper channel,
	    PodcastRepository db) {
		addChannelToSubscriptionsStore(channel.getUrl());
		db.insertChannel(channel);
		mSubsUpdate.updateListView(this);
		mSubscriptionsUpdated = false;
		makeToast("Subscription added");
	}


	@Override
	public boolean subscriptionsUpdated() {
		boolean result = mSubscriptionsUpdated;
		mSubscriptionsUpdated = false;
		return result;
	}
	
	private void deleteChannelFromSubscriptionsStore(String channelUrl) {
		ServiceCallInitialiserTask svcInitialiser = new ServiceCallInitialiserTask(
				this, _prefs);
		String url = "https://podcastapi.azurewebsites.net/breeze/subscriptions/delete";
		ServiceRequest request = new ServiceRequest(RequestMethod.POST, url);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("", channelUrl));
		request.setmParams(params);
		svcInitialiser.execute(request);
	}
	
	public void addChannelToSubscriptionsStore(String channelUrl) {
		ServiceCallInitialiserTask svcInitialiser = new ServiceCallInitialiserTask(
				this, _prefs);
		String url = "https://podcastapi.azurewebsites.net/breeze/subscriptions/add";
		ServiceRequest request = new ServiceRequest(RequestMethod.POST, url);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("", channelUrl));
		request.setmParams(params);
		svcInitialiser.execute(request);
	}

	// Callback from svcInitialiser
	@Override
	public void onTaskCompleted(HttpRequestBase request) {
		ServiceHandlerTask svcHandler = new ServiceHandlerTask(this, null);
		svcHandler.execute(request);
	}

	// Callback from svcHandler
	@Override
	public void onTaskCompleted(HttpResponseViewModel objResponse) {
		String revisionId = null;
		RevisionManager revManager = new RevisionManager();
		JSONObject json;
		try {
			json = new JSONObject(objResponse.getResponseBody());
		
		revisionId = json.getString("RevisionId"); 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		revManager.updateRevision(_prefs, revisionId);
		makeToast("Revision updated to: " + revisionId);
		
	}
	
	@Override
	public void makeToast(String message) {
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, message, duration);
		toast.show();
	}
	
	@Override
	public boolean haveNetworkConnection() {
		 boolean haveConnectedWifi = false;
		    boolean haveConnectedMobile = false;
		    boolean haveConnection = false;

		    ConnectivityManager cm = (ConnectivityManager) 
		    		getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		    for (NetworkInfo ni : netInfo) {
		        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
		            if (ni.isConnected())
		                haveConnectedWifi = true;
		        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
		            if (ni.isConnected())
		                haveConnectedMobile = true;
		    }
		    haveConnection = haveConnectedWifi || haveConnectedMobile;
		    
		    if(!haveConnection) 
		    	makeToast("No internet connection present, this function requires a mobile or Wifi connection.");
		    
		    return haveConnection;
	}
	
	void doBindService() {
	    // Establish a connection with the service.  We use an explicit
	    // class name because we want a specific service implementation that
	    // we know will be running in our own process (and thus won't be
	    // supporting component replacement by other applications).
		bindService(new Intent(MainActivity.this, 
	            PlayerService.class), mConnection, 0);
	    
	}
	
	void doUnbindService() {
	    if (mIsBound) {
	        // Detach our existing connection.
	        unbindService(mConnection);
	        mIsBound = false;
	    }
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {
	    public void onServiceConnected(ComponentName className, IBinder service) {
	        // This is called when the connection with the service has been
	        // established, giving us the service object we can use to
	        // interact with the service.  Because we have bound to a explicit
	        // service that we know is running in our own process, we can
	        // cast its IBinder to a concrete class and directly access it.
	        MainActivity.this.mBoundService = ((PlayerService.LocalBinder)service).getService();
	        MainActivity.this.mIsBound = true;
	        MainActivity.this.mBoundService.connectPlaylistUIActivity(MainActivity.this);
	        MainActivity.this.invalidateOptionsMenu();
	       /* if(!MainActivity.this.mBoundService.mp.isPlaying()){
	        	mPlaylistManager.deleteCurrentlyPlayingUrl(_prefs);
	        	mPlayList.playerIsStopped(MainActivity.this);
	        }*/
	        MainActivity.this.makeToast("Main Activity Connected to Player Service");

				
	        }

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			MainActivity.this.mBoundService = null;
			 MainActivity.this.makeToast("Main Activity Disconnected from Player Service");
		}
	    };
	    
		@Override
		protected void onDestroy() {
		    super.onDestroy();
		    doUnbindService();
		}


		@Override
		public void playerIsNowPlayingUrl(String url, Context context) {
			mCurrentEpisodeUrl = url;
			if(mPlayList != null)
				mPlayList.playerIsNowPlayingUrl(url, this);			
		}


		@Override
		public void playerIsStopped(Context context) {
			if(mPlayList != null)
				mPlayList.playerIsStopped(this);
			invalidateOptionsMenu();
		}



		@Override
		public void setCurrentEpisodeUrl(String url) {
			mCurrentEpisodeUrl = url;			
		}

		@Override
		public void stopPlayer() {
			try{
				if(mIsBound){
					mBoundService.mp.stop();
					mBoundService.stopSelf();
				}
			}
			catch(Exception e){
				makeToast(e.getMessage());
			}
			
		}
		
		protected void openPlayer() {
			startActivity(new Intent(this, PlayerActivity.class));
		}
		

}

