package com.toastworks.podimio;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.toastworks.podimio.core.PlayListManager;
import com.toastworks.podimio.interfaces.ListViewUpdateListener;
import com.toastworks.podimio.interfaces.PlayerStopper;
import com.toastworks.podimio.interfaces.PlayerUpdateListener;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimio.model.PlaylistItemViewModel;
import com.toastworks.podimio.persistence.PodcastRepository;
import com.toastworks.podimio.view.PlaylistAdapter;
import com.toastworks.podimo.R;
import com.toastworks.utilities.Utilities;

public class PlaylistFragment extends ListFragment implements ListViewUpdateListener, PlayerUpdateListener {
	private PlayListManager mPlaylistManager;
	private SharedPreferences mPrefs;
	private View mRootView;
	private PlaylistAdapter mAdapter; 
	private ToastMaker mToastMaker;
	private PlayerStopper mPlayerStopper;
	private ArrayList<PlaylistItemViewModel> mPlaylist;
	private PodcastRepository mRepository;
	private Utilities mUtils;
	private String currentEpisodeUrl = "";

	
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mToastMaker = (ToastMaker) activity;
			mPlayerStopper = (PlayerStopper) activity;
		} catch (ClassCastException castException) {
			castException.printStackTrace();
		}
		
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		mUtils = new Utilities();
        mRootView = inflater.inflate(R.layout.fragment_main_playlist, container, false);
        setHasOptionsMenu(true);
        
        Button playButton = getPlayButton();
    	playButton.setOnClickListener(new View.OnClickListener() {
    	    @Override
    	    public void onClick(View v) {
    	    	PlaylistFragment.this.startPlayerForPlayList(0);
    	    }
    	});
    	
    	Button stopButton = getStopButton();
    	stopButton.setOnClickListener(new View.OnClickListener() {
    	    @Override
    	    public void onClick(View v) {
    	    	mPlayerStopper.stopPlayer();
    	    	PlaylistFragment.this.hidePlayButtonIfPlayListEmpty();
    	    }
    	});
        
        return mRootView;
    }

	


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mPlaylistManager = new PlayListManager();
		mPrefs = getActivity().getSharedPreferences(
			      "com.toastworks.podimo", Context.MODE_PRIVATE);
		Context context = getActivity().getApplicationContext();
		 mPlaylist = new ArrayList<PlaylistItemViewModel>();
		getPlayListFromDb(context);
		
		currentEpisodeUrl = mPlaylistManager.getCurrentlyPlayingUrl(mPrefs);
		
		if(currentEpisodeUrl != null)
		   playerIsNowPlayingUrl(currentEpisodeUrl, context);
		
		mAdapter = new PlaylistAdapter(getActivity().getBaseContext(), 
				R.layout.playlist_list_item, mPlaylist);

		setListAdapter(mAdapter);

		ListView listView = getListView();
		registerForContextMenu(listView); 
		
		listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                int position, long id) {
            	PlaylistItemViewModel item = (PlaylistItemViewModel) parent.getItemAtPosition(position);  
            	if(item.isPlaying()) PlaylistFragment.this.openPlayer();
            }});
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    // Inflate the menu items for use in the action bar
	    inflater.inflate(R.menu.playlist_fragment_actions, menu);
	    
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.clear_playlist:
			clearPlayList();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void clearPlayList() {
		for(int i = 0; i < mPlaylist.size(); i++){
			PlaylistItemViewModel selectedEpisode = mPlaylist.get(i);
			 mRepository.deletePlaylistItem(selectedEpisode);		 
		}
		updateListView(getActivity());
		
	}

	private void hidePlayButtonIfPlayListEmpty() {
		if(mPlaylist.size() == 0) getPlayButton().setVisibility(View.GONE);
	}
	
	protected void openPlayer() {
		startActivity(new Intent(getActivity(), PlayerActivity.class));
		getActivity().finish();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		getActivity().getMenuInflater().inflate(R.menu.playlist_item_menu, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.remove_episode:
			 removeEpisode(item);
			return true;
		case R.id.play_from_here:
			startPlayerForPlayList(getMenuItemIndex(item));	
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void removeEpisode(MenuItem item) {
		int episodeIndex = getMenuItemIndex(item);
		 PlaylistItemViewModel selectedEpisode = mPlaylist.get(episodeIndex);
		 mRepository.deletePlaylistItem(selectedEpisode);
		 updateListView(getActivity());
	}

	private int getMenuItemIndex(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		 int episodeIndex = (int) info.id;
		return episodeIndex;
	}

	protected void startPlayerForPlayList(int index) {
		if(mUtils.haveNetworkConnection(getActivity())){
			ArrayList<String> episodesList = new ArrayList<String>();
			for(int i = index; i < mPlaylist.size(); i++){
				episodesList.add(mPlaylist.get(i).getUrl());
			}
			Intent intent = new Intent(getActivity().getBaseContext(), PlayerActivity.class);
			 intent.putStringArrayListExtra("episodesList", episodesList);
		   	 startActivity(intent);
		   	 getActivity().finish();
		}
		else mToastMaker.makeToast("No internet connection available - streaming not possible");
	}


	@Override
	public void updateListView(Context context) {
		getPlayListFromDb(context);	
		mAdapter.notifyDataSetChanged();
	}
	
	private void getPlayListFromDb(Context context) {	
		mRepository = getRepository(context);
		mPlaylist.clear();
        mPlaylist.addAll(mRepository.getPlayList());
        hidePlayButtonIfPlayListEmpty();
	}
	
	
	
	private Button getPlayButton() {
		return (Button) mRootView.findViewById(R.id.play_playlist_btn);
	}
	
	private Button getStopButton() {
		return (Button) mRootView.findViewById(R.id.stop_playlist_btn);
	}
	
	private void showStopButton(){
		getPlayButton().setVisibility(View.GONE);
		getStopButton().setVisibility(View.VISIBLE);
	}
	
	private void showPlayButton(){
		getPlayButton().setVisibility(View.VISIBLE);
		getStopButton().setVisibility(View.GONE);
	}

	@Override
	public void playerIsNowPlayingUrl(String url, Context context) {
		showStopButton();
		refreshPlayListIfNecessary(context);
		if(mAdapter == null) setUpListView(context);
		if(url != ""){
			currentEpisodeUrl = url;
			resetIsPlayingFlags();
			for(int i = 0; i < mPlaylist.size(); i++){
				if(mPlaylist.get(i).getUrl().equals(url)) {
					mPlaylist.get(i).isPlaying(true);
					break;
				}
				
			}
			mAdapter.notifyDataSetChanged();
		}
	}

	private void refreshPlayListIfNecessary(Context context) {
		if(mPlaylist == null){
			getPlayListFromDb(context);
		}
	}
	
	@Override
	public void playerIsStopped(Context context) {
		refreshPlayListIfNecessary(context);
		if(mAdapter != null){
			resetIsPlayingFlags();
			mAdapter.notifyDataSetChanged();
		}
		showPlayButton();
		hidePlayButtonIfPlayListEmpty();
	}

	private void resetIsPlayingFlags() {
		for(PlaylistItemViewModel item : mPlaylist){
			item.isPlaying(false);
		}
	}
	
    public PodcastRepository getRepository(Context context){
		if(mRepository == null) return new PodcastRepository(context);
		else return mRepository;
	}
    
    private void setUpListView(Context context){
    	mAdapter = new PlaylistAdapter(context, 
				R.layout.playlist_list_item, mPlaylist);

		setListAdapter(mAdapter);

		ListView listView = getListView();
		registerForContextMenu(listView); 
    }

	@Override
	public void setCurrentEpisodeUrl(String url) {
		// TODO Auto-generated method stub
		
	}
	
	

	
	
	
	
	
}
