package com.toastworks.podimio.interfaces;


public interface FragmentManipulator {
	void hideFragments();
}
