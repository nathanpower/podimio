package com.toastworks.podimio.interfaces;

public interface PlayerUiUpdateListener {
	void startProgressBar();
	void stopProgressBar();
	void hideStreamProgressBar();
	void showStreamProgressBar();
	void makeToast(String message);
	void displayEpisodeTitle(String currentEpisodeUrl);
	void changeStateToPaused();
	
}
