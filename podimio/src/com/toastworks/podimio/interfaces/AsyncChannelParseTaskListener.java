package com.toastworks.podimio.interfaces;

import com.toastworks.podimio.model.FeedChannelJsonMapper;

public interface AsyncChannelParseTaskListener {
	void onTaskCompleted(FeedChannelJsonMapper channel);
}
