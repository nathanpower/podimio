package com.toastworks.podimio.interfaces;

public interface NetworkCheckCompleteListener<Boolean> {
	void onNetworkCheckComplete(boolean result);
}
