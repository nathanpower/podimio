package com.toastworks.podimio.interfaces;

import android.content.Context;

public interface PlayerUpdateListener {
	void playerIsNowPlayingUrl(String url, Context context);
	void playerIsStopped(Context context);
	void setCurrentEpisodeUrl(String url);
}
