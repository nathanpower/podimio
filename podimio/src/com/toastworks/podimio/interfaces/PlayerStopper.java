package com.toastworks.podimio.interfaces;

public interface PlayerStopper {
	void stopPlayer();
}
