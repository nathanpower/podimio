package com.toastworks.podimio.interfaces;

public interface AccountInputValidator {
	public boolean inputIsValid(String username, String password, String confirmPassword);
}
