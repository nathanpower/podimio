package com.toastworks.podimio.interfaces;

import com.toastworks.podimio.model.FeedChannelJsonMapper;

public interface ChannelSelectedListener {
	public void onChannelSelected(FeedChannelJsonMapper result);
}

