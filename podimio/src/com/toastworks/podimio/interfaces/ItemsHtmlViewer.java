package com.toastworks.podimio.interfaces;

import java.util.List;
import java.util.Map;

public interface ItemsHtmlViewer {
	void onItemsHtmlParsed(List<Map<String, CharSequence>> itemsViewData);
	void onItemHtmlParsed(CharSequence itemViewData);
}
