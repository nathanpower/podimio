package com.toastworks.podimio.interfaces;

import org.apache.http.client.methods.HttpRequestBase;

public interface AsyncSvcCallInitListener {
	void onTaskCompleted(HttpRequestBase channel);
}
