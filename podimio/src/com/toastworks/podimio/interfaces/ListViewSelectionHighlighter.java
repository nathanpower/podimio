package com.toastworks.podimio.interfaces;

import android.view.View;
import android.widget.ListView;

public interface ListViewSelectionHighlighter {
	void toggleSelection(ListView parent, View view);
}
