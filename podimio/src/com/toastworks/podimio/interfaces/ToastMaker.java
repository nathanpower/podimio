package com.toastworks.podimio.interfaces;

public interface ToastMaker {
	public void makeToast(String message);
}
