package com.toastworks.podimio.interfaces;

import android.view.View;

import com.toastworks.podimio.model.FeedItemDao;

public interface EpisodeSelectedListener {
	public void onItemSelected(FeedItemDao item);
	public void playEpisode();
	public void addEpisodeToPlayList(View v);
}
