package com.toastworks.podimio.interfaces;

public interface NetworkChecker {
	public boolean haveNetworkConnection();
}
