package com.toastworks.podimio.interfaces;

import android.content.Context;

public interface ListViewUpdateListener {
	public void updateListView(Context context);
}
