package com.toastworks.podimio.interfaces;

import com.toastworks.podimio.model.HttpResponseViewModel;

public interface AsyncServiceTaskListener {
	void onTaskCompleted(HttpResponseViewModel objResponse);
}
