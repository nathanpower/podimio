package com.toastworks.podimio.interfaces;

import com.toastworks.podimio.model.FeedItemDao;

public interface EpisodeManager {
	void startPlayerForEpisode(FeedItemDao item);
	void addEpisodeToPlayList(FeedItemDao episode);
	void checkForNewEpisodes();
}
