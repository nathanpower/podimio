package com.toastworks.podimio.interfaces;

import com.toastworks.podimio.model.FeedChannelJsonMapper;

public interface SubscriptionManager {
	public void addSubscription(FeedChannelJsonMapper channel);
	public void deleteSubscription(String url);
	public boolean subscriptionsUpdated();
}
