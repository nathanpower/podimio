package com.toastworks.podimio.interfaces;

import java.util.ArrayList;

public interface PlaylistRepository {
	ArrayList<String> getPlayListUrls(); 
}
