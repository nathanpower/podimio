package com.toastworks.podimio;


import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.toastworks.podimio.core.AccountManager;
import com.toastworks.podimio.core.TokenManager;
import com.toastworks.podimio.interfaces.AccountInputValidator;
import com.toastworks.podimio.interfaces.AsyncServiceTaskListener;
import com.toastworks.podimio.interfaces.NetworkChecker;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimio.model.HttpResponseViewModel;
import com.toastworks.podimo.R;

public class LoginFragment extends Fragment implements AsyncServiceTaskListener {
	private View mRootView;
	private SharedPreferences mPrefs;
	private TokenManager mTokenManager = new TokenManager();
	private ToastMaker mToastMaker;
	private AccountInputValidator mValidator;
	private NetworkChecker mNetworkChecker;
	
	
	
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		 try {
             mToastMaker = (ToastMaker) activity;
             mValidator = (AccountInputValidator) activity;
             mNetworkChecker = (NetworkChecker) activity;
         } catch (ClassCastException castException) {
        	 castException.printStackTrace();
         }
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		 mPrefs = getActivity().getSharedPreferences(
			      "com.toastworks.podimo", Context.MODE_PRIVATE);
		 
        mRootView = inflater.inflate(R.layout.fragment_login, container, false);
        
        Button button = getSignOnButton();
    	button.setOnClickListener(new View.OnClickListener() {
    	    @Override
    	    public void onClick(View v) {
    	       processLogin();
    	    }
    	});
        
        return mRootView;
    }

	private Button getSignOnButton() {
		return (Button) mRootView.findViewById(R.id.sign_in);
	}

	protected void processLogin() {
		ProgressBar progressBar = getProgressBar();
		AccountManager accountManager = new AccountManager(this, progressBar);
		final EditText nameField = getUsernameField();  
		String username = nameField.getText().toString();  
		final EditText passwordField = getPasswordField();  
		String password = passwordField.getText().toString(); 
		boolean haveNetworkConnection = mNetworkChecker.haveNetworkConnection();
		boolean inputIsValid = mValidator.inputIsValid(username, password, null);
		
		if(haveNetworkConnection && inputIsValid)
		{	
			getSignOnButton().setVisibility(View.GONE);
			progressBar.setVisibility(View.VISIBLE);
			// Call AsyncTask, which calls onTaskCompleted when done
			accountManager.SignIn(username, password);		
		}
	}

	private EditText getPasswordField() {
		return (EditText) mRootView.findViewById(R.id.login_password);
	}

	private EditText getUsernameField() {
		return (EditText) mRootView.findViewById(R.id.login_username);
	}

	
	@Override
	public void onTaskCompleted(HttpResponseViewModel response) {
		JSONObject resultJson;
		String token;
		Intent intent;
		getSignOnButton().setEnabled(true);
		// makeToast(response.getResponseCodeString() + ": "+ response.getResponseBody());
		
		if(response.getResponseCode() == 200) {
			resultJson = convertStringToJson(response.getResponseBody());
			token = getTokenFromJson(resultJson);
			mTokenManager.storeToken(mPrefs, token);
			mToastMaker.makeToast("Sign in successful");
			intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
		    startActivity(intent);
		    getActivity().finish();
			
		}
		
		else{
			getPasswordField().setText("");
			getUsernameField().setText("");
			getSignOnButton().setVisibility(View.VISIBLE);
			mToastMaker.makeToast("Error: " + response.getResponseCodeString() + ": "+ response.getResponseBody());
		}
		
	}
	
	private String getTokenFromJson(JSONObject resultJson) {
		try {
			return resultJson.getString("access_token");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private JSONObject convertStringToJson(String input){
		JSONObject jsonResult = null;
		try {
			jsonResult = new JSONObject(input);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonResult;
		
	}
	
	private ProgressBar getProgressBar(){
		return (ProgressBar) mRootView.findViewById(R.id.sign_in_progress_bar);
	}
	
	
	
	
}