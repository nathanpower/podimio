package com.toastworks.podimio.view;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.toastworks.podimio.core.PlayListManager;
import com.toastworks.podimio.model.PlaylistItemViewModel;
import com.toastworks.podimo.R;


public class PlaylistAdapter extends ArrayAdapter {
	 private SharedPreferences mPrefs;
	 private PlayListManager mPlaylistManager;
	 private String mPlaybackStateImage;
	 private ArrayList<PlaylistItemViewModel> items;
	 private final Context context;
     public PlaylistAdapter(Context context, int textViewResourceId, ArrayList<PlaylistItemViewModel> items) {
             super(context, textViewResourceId, items);
             this.context = context;
             this.items = items;
             mPrefs = context.getSharedPreferences(
   			      "com.toastworks.podimo", Context.MODE_PRIVATE);
             mPlaylistManager = new PlayListManager();
             mPlaybackStateImage = mPlaylistManager.getPlaybackStateImage(mPrefs);
     }

     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
       LayoutInflater inflater = (LayoutInflater) context
           .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       View rowView = inflater.inflate(R.layout.playlist_list_item, parent, false);
       TextView heading = (TextView) rowView.findViewById(R.id.playListHeading);
       TextView description = (TextView) rowView.findViewById(R.id.playListDescription);
       ImageView imageView = (ImageView) rowView.findViewById(R.id.play_icon);
       // Change the icon for Windows and iPhone
       PlaylistItemViewModel item = items.get(position);
       
       heading.setText(item.getFeedTitle());
       description.setText(item.getEpisodeTitle());
       
       if(mPlaybackStateImage != null){
    	   imageView.setImageResource(Integer.parseInt(mPlaybackStateImage));
		}
       
       if(item.isPlaying())
    	   imageView.setVisibility(View.VISIBLE);
       else imageView.setVisibility(View.GONE);
 
       return rowView;
     }
}
