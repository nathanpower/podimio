package com.toastworks.podimio;

import java.util.List;
import java.util.Map;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.toastworks.podimio.asynctasks.ItemHtmlParserTask;
import com.toastworks.podimio.interfaces.ItemsHtmlViewer;
import com.toastworks.podimo.R;

public class EpisodeDetailFragment extends Fragment implements ItemsHtmlViewer{
	
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 100;
	
	private ProgressBar mProgressBar;
	private CharSequence mEpisodeViewData;
	private String mEpisodeTitle;
	
	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	      Bundle savedInstanceState) {
	    View view = inflater.inflate(R.layout.fragment_episode_detail,
	        container, false);
	    mProgressBar = (ProgressBar) view.findViewById(R.id.episode_progressbar);
	    return view;
	  }

	  @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		TextView descriptionView = (TextView) getView().findViewById(R.id.episode_details_text);
		TextView titleView = (TextView) getView().findViewById(R.id.episode_detail_title);
		titleView.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);
		descriptionView.setMovementMethod(new ScrollingMovementMethod());
		
		if(savedInstanceState != null){
			mEpisodeViewData = savedInstanceState.getCharSequence("EpisodeDetails");
			mEpisodeTitle = savedInstanceState.getString("EpisodeTitle");

		}
		
	}
	
	

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putCharSequence("EpisodeDetails", mEpisodeViewData);
		outState.putString("EpisodeTitle", mEpisodeTitle);

	}

	public void setText(String description, String title) {
		TextView titleView = (TextView) getView().findViewById(R.id.episode_detail_title);
		titleView.setText(title);
		
		if(mEpisodeViewData == null || !(mEpisodeTitle.equals(title))){
			ItemHtmlParserTask htmlParser = new ItemHtmlParserTask(this);
			getTextView().setText("");
			mProgressBar.setVisibility(View.VISIBLE);
			htmlParser.execute(description);
		}
		else {
			TextView descriptionView = getTextView();
			descriptionView.setText(mEpisodeViewData);
		}
		mEpisodeTitle = title;
	  }

	@Override
	public void onItemsHtmlParsed(List<Map<String, CharSequence>> itemsViewData) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onItemHtmlParsed(CharSequence itemViewData) {
		mProgressBar.setVisibility(View.GONE);
		mEpisodeViewData = itemViewData;
		TextView descriptionView = getTextView();
		
		if(itemViewData != null && descriptionView != null)
			descriptionView.setText(itemViewData);
		else if(itemViewData == null && descriptionView != null)
			descriptionView.setText("Could not retrieve description for episode");
			
		
	}
	
	public TextView getTextView(){
		try{
			return (TextView) getView().findViewById(R.id.episode_details_text);
		}
		catch(Exception e){
			return null; // user has moved to other activity before callback
		}
	}


}
