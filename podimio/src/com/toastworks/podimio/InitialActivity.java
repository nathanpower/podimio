package com.toastworks.podimio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.toastworks.podimio.core.TokenManager;
import com.toastworks.podimo.R;

public class InitialActivity extends Activity {

	Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.initial_layout);
		
		 SharedPreferences prefs = this.getSharedPreferences(
			      "com.toastworks.podimo", Context.MODE_PRIVATE);
		TokenManager tokenManager = new TokenManager();
		
		
		
		if(tokenManager.tokenExists(prefs)) {
			 intent = new Intent(getApplicationContext(), MainActivity.class);
			    startActivity(intent);			    
		}
		else {
			intent = new Intent(getApplicationContext(), AccountActivity.class);
		    startActivity(intent);
		}
		
		finish();
	}
	

}
