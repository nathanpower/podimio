package com.toastworks.podimio;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.HttpRequestBase;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.toastworks.podimio.asynctasks.CheckNetworkTask;
import com.toastworks.podimio.asynctasks.ServiceCallInitialiserTask;
import com.toastworks.podimio.asynctasks.ServiceHandlerTask;
import com.toastworks.podimio.core.RevisionManager;
import com.toastworks.podimio.interfaces.AsyncServiceTaskListener;
import com.toastworks.podimio.interfaces.AsyncSvcCallInitListener;
import com.toastworks.podimio.interfaces.ListViewUpdateListener;
import com.toastworks.podimio.interfaces.NetworkCheckCompleteListener;
import com.toastworks.podimio.interfaces.NetworkChecker;
import com.toastworks.podimio.interfaces.SubscriptionManager;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimio.model.FeedChannelDao;
import com.toastworks.podimio.model.HttpResponseViewModel;
import com.toastworks.podimio.model.RequestMethod;
import com.toastworks.podimio.model.ServiceRequest;
import com.toastworks.podimio.persistence.PodcastRepository;
import com.toastworks.podimio.services.SyncService;
import com.toastworks.podimo.R;

public class SubscriptionsFragment extends ListFragment implements ListViewUpdateListener, 
AsyncSvcCallInitListener, AsyncServiceTaskListener, NetworkCheckCompleteListener<Boolean>{
	private View mRootView;
	private LayoutInflater mInflater;
	private ArrayAdapter<FeedChannelDao> mAdapter; 
	private SubscriptionManager mSubscriptionManager;
	private List<FeedChannelDao> mChannelList= new ArrayList<FeedChannelDao>();
	private List<FeedChannelDao> mStagingList= new ArrayList<FeedChannelDao>();
	private static final String CHANNELS_KEY = "SubscribedChannels";
	private String mFeedUrlKey;
	private String mFeedTitleKey;
	private SharedPreferences mPrefs;
	private SubApplication mApp;
	private ToastMaker mToastMaker;
	private NetworkChecker mNetworkChecker;
	private BroadcastReceiver receiver = new BroadcastReceiver() {

		    @Override
		    public void onReceive(Context context, Intent intent) {
		      Bundle bundle = intent.getExtras();
				int duration = Toast.LENGTH_LONG;
				Toast toast = Toast.makeText(context, (CharSequence) bundle.get("Success"), duration);
				toast.show();
				mApp.hasRefreshed = true;
				getSubscribedChannelsFromDb();
		    }
		  };
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		 try {
             mSubscriptionManager = (SubscriptionManager) activity;
             mToastMaker = (ToastMaker) activity;
             mNetworkChecker = (NetworkChecker) activity;
         } catch (ClassCastException castException) {
        	 castException.printStackTrace();
         }
		 
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		mFeedUrlKey = getActivity().getBaseContext().getString(R.string.feed_url_key);
		mFeedTitleKey = getActivity().getBaseContext().getString(R.string.feed_title_key);	
		mInflater = inflater;
        mRootView = inflater.inflate(R.layout.fragment_main_subscriptions, container, false);
        mPrefs = getActivity().getSharedPreferences("com.toastworks.podimo",
				Context.MODE_PRIVATE);
        if(savedInstanceState == null){
        	getSubscribedChannelsFromDb();
        	mApp = (SubApplication ) getActivity().getApplicationContext();
        	
        	if(!mApp.hasRefreshed){
        		mApp.hasRefreshed = true;
        		testConnectionPreUpdateCheck();
        	}
        }
        else if(mSubscriptionManager.subscriptionsUpdated()) getSubscribedChannelsFromDb();
        else mChannelList = savedInstanceState.getParcelableArrayList(CHANNELS_KEY);
         
        return mRootView;
    }
	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		 mAdapter = new ArrayAdapter<FeedChannelDao>(  
	   		     mInflater.getContext(), R.layout.podcast_list_item,  
	   		     mChannelList);  
	   		   setListAdapter(mAdapter);  
	   		   
	   		ListView listView = getListView();
			registerForContextMenu(listView);
			
			listView.setOnItemClickListener(new OnItemClickListener() {
	              public void onItemClick(AdapterView<?> parent, View view,
	                  int position, long id) {
	            	  	
	            	  Intent intent = new Intent(getActivity().getBaseContext(), EpisodesActivity.class);
	            	  intent.putExtra(mFeedUrlKey, mChannelList.get(position).getUrl());
	            	  intent.putExtra(mFeedTitleKey, mChannelList.get(position).getTitle());
	            	  startActivity(intent);
	            	  
	              }
	});


	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putParcelableArrayList(CHANNELS_KEY, (ArrayList<? extends Parcelable>) mChannelList);
        super.onSaveInstanceState(outState);
    }

	
	  @Override
	public void onResume() {
	    super.onResume();
	    getActivity().registerReceiver(receiver, new IntentFilter(SyncService.NOTIFICATION));
	  }
	  @Override
	public void onPause() {
	    super.onPause();
	    getActivity().unregisterReceiver(receiver);
	  }
	  
	  @Override
		public void onCreateContextMenu(ContextMenu menu, View v,
				ContextMenuInfo menuInfo) {
			getActivity().getMenuInflater().inflate(R.menu.subscriptions_menu, menu);
		}

		@Override
		public boolean onContextItemSelected(MenuItem item) {

			switch (item.getItemId()) {
			case R.id.unsubscribe:
				AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
				mSubscriptionManager.deleteSubscription(mChannelList.get(info.position).getUrl());
				return true;

			default:
				return super.onOptionsItemSelected(item);
			}
		}
	
	private void getSubscribedChannelsFromDb() {
		PodcastRepository db = new PodcastRepository(getActivity().getBaseContext());
		try{
			mChannelList.clear();
			mStagingList = db.getAllChannels();	
			mChannelList.addAll(mStagingList);
			if(mAdapter != null)
				mAdapter.notifyDataSetChanged();
		} catch( SQLiteException e){
			// Tables corrupt or missing
			e.printStackTrace();
		}
	}

	@Override
	public void updateListView(Context context) {
		getSubscribedChannelsFromDb();	
		mAdapter.notifyDataSetChanged();
	}
	
	// Android connection check unreliable on emulator
		// Using this async check before long background tasks during development
		public void testConnectionPreUpdateCheck() {
			CheckNetworkTask task = new CheckNetworkTask(this);
	        String URL = "http://www.google.com";
	        task.execute(new String[]{URL});
	    }

		@Override
		public void onNetworkCheckComplete(boolean connectionAvailable) {
			if(connectionAvailable)	
				prepareRequestForUpdateCheck();
			else {
				mToastMaker.makeToast("No internet connection available - sync cancelled");
				mApp.hasRefreshed = false;
			}
		}

	private void prepareRequestForUpdateCheck() {
		//if(mNetworkChecker.haveNetworkConnection()){			
			ServiceCallInitialiserTask svcInitialiser = new ServiceCallInitialiserTask(
					this, mPrefs);
			String url = "https://podcastapi.azurewebsites.net/breeze/subscriptions/revision";
			ServiceRequest request = new ServiceRequest(RequestMethod.GET, url);
			svcInitialiser.execute(request);
		//}
	}

	// Callback from svcInitialiser
	@Override
	public void onTaskCompleted(HttpRequestBase request) {
		ServiceHandlerTask svcHandler = new ServiceHandlerTask(this, null);
		svcHandler.execute(request);
		
	}
	
	// Callback from svcHandler
	@Override
	public void onTaskCompleted(HttpResponseViewModel objResponse) {
		String revisionId = null;
		RevisionManager revManager = new RevisionManager();
		String currentRevisionId = revManager.getSubscriptionRevision(mPrefs);
		JSONObject json;
		try {
			json = new JSONObject(objResponse.getResponseBody());
		
		revisionId = json.getString("RevisionId"); 
		} catch (JSONException e) {
			mApp.hasRefreshed = false;
			mToastMaker.makeToast("Subscription Sync Failed");
			e.printStackTrace();
		}
		
		// Start syncing service if revision number not the same
		if(revisionId == null || !revisionId.equals(currentRevisionId)){
			 mToastMaker.makeToast("Starting background sync");
			 Intent intent = new Intent(getActivity(), SyncService.class);
			 intent.putExtra("revisionId", revisionId);
			    getActivity().startService(intent);
			    			    
		}
		else if(revisionId != null && revisionId.equals(currentRevisionId)){
			mToastMaker.makeToast("Subscriptions are up to date");
		}
		
	}
	
	
	
	

	
	
	
}
