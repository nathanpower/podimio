package com.toastworks.podimio;

import java.util.Locale;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.widget.Toast;

import com.toastworks.podimio.interfaces.AccountInputValidator;
import com.toastworks.podimio.interfaces.NetworkChecker;
import com.toastworks.podimio.interfaces.ToastMaker;
import com.toastworks.podimo.R;

public class AccountActivity extends FragmentActivity implements
		ActionBar.TabListener,  AccountInputValidator, ToastMaker, NetworkChecker {
	
	SharedPreferences _prefs;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		 _prefs = this.getSharedPreferences(
			      "com.toastworks.podimo", Context.MODE_PRIVATE);
		
		setContentView(R.layout.activity_account);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.account_pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
		
		

	    
	}



	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			
			switch (position) {
	        case 0:
	            return new LoginFragment();
	        case 1:
	            return new RegisterFragment();

	        }
	 
	        return null;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.login_section1).toUpperCase(l);
			case 1:
				return getString(R.string.register_section2).toUpperCase(l);

			}
			return null;
		}
	}

	@Override
	public void makeToast(String message) {
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_LONG;		
		Toast toast = Toast.makeText(context, message, duration);
		toast.show();
	}



	@Override
	public boolean inputIsValid(String username, String password, String confirmPassword) {
		if(TextUtils.isEmpty(username)) {
			makeToast("Username is required!");
			return false;
		}
		else if(username.contains(" ")){
			makeToast("Spaces are not allowed in username!");
			return false;
		}else if(TextUtils.isEmpty(password)) {
			makeToast("Password is required");
			return false;
		}
		else if(password.contains(" ")){
			makeToast("Spaces are not allowed in password!");
			return false;
		}
		else if(password.length() < 6) {			
			makeToast("Password must have 6 or more characters!");
			return false;
		}
		if(confirmPassword != null)
			if(!confirmPassword.equals(password)) {
				makeToast("Passwords do not match!");
				return false;
			}
		
		return true;
	}



	@Override
	public boolean haveNetworkConnection() {
		 boolean haveConnectedWifi = false;
		    boolean haveConnectedMobile = false;
		    boolean haveConnection = false;

		    ConnectivityManager cm = (ConnectivityManager) 
		    		getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		    for (NetworkInfo ni : netInfo) {
		        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
		            if (ni.isConnected())
		                haveConnectedWifi = true;
		        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
		            if (ni.isConnected())
		                haveConnectedMobile = true;
		    }
		    haveConnection = haveConnectedWifi || haveConnectedMobile;
		    
		    if(!haveConnection) 
		    	makeToast("No internet connection present, this function requires a mobile or Wifi connection.");
		    
		    return haveConnection;
	}




	
}
