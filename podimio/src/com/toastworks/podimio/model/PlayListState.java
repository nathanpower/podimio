package com.toastworks.podimio.model;

import java.util.ArrayList;

public class PlayListState {

	private ArrayList<String> episodeList;
	private int currentEpisode;
	
	public PlayListState(ArrayList<String> episodeList,int currentEpisode ) {
		this.episodeList = episodeList;
		this.currentEpisode = currentEpisode;
	}
	
	public ArrayList<String> getEpisodeList() {
		return episodeList;
	}
	public int getCurrentEpisode() {
		return currentEpisode;
	}
	
}
