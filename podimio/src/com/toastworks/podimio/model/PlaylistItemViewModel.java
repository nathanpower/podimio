package com.toastworks.podimio.model;

public class PlaylistItemViewModel {
	private String url;
	private String feedTitle;
	private String episodeTitle;
	private boolean isPlaying = false;
	
	public PlaylistItemViewModel() {
		
	}
	
	public PlaylistItemViewModel(FeedItemDao episode, String feedTitle) {
		this.url = episode.getUrl();
		this.episodeTitle = episode.getTitle();
		this.feedTitle = feedTitle;
	}
	
	
	public void setUrl(String url) {
		this.url = url;
	}
	public void setFeedTitle(String feedTitle) {
		this.feedTitle = feedTitle;
	}
	public void setEpisodeTitle(String episodeTitle) {
		this.episodeTitle = episodeTitle;
	}
	public void isPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}
	public String getUrl() {
		return url;
	}
	public String getFeedTitle() {
		return feedTitle;
	}
	public String getEpisodeTitle() {
		return episodeTitle;
	}
	public boolean isPlaying() {
		return isPlaying;
	}
	
	
}
