package com.toastworks.podimio.model;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

public class HttpResponseViewModel {

	private Integer _responseCode;
	private String _responseBody;
	
	public HttpResponseViewModel(HttpResponse response){
		_responseCode = response.getStatusLine().getStatusCode();
		try {
			_responseBody = EntityUtils.toString(response.getEntity());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getResponseCode() {
		return (int)_responseCode;
	}

	public String getResponseBody() {
		return _responseBody;
	}

	public CharSequence getResponseCodeString() {
		return _responseCode.toString();
	}
	
}
