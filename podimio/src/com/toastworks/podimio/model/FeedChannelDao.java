package com.toastworks.podimio.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedChannelDao implements Parcelable{
	private String url;
	private String title;
	private String description;
	
	public FeedChannelDao() {}
	
	private FeedChannelDao(Parcel in) {
		in.writeString(url);
        in.writeString(title);
        in.writeString(description);
    }

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
			
	final Parcelable.Creator CREATOR = new Parcelable.Creator() {
	        public FeedChannelDao createFromParcel(Parcel in) { return new FeedChannelDao(in); }
	        public FeedChannelDao[] newArray(int size) { return new FeedChannelDao[size]; }
	    };
	}
	
	
	
	@Override
	public String toString() {
		return title;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int flags) {
        out.writeString(url);
        out.writeString(title);
        out.writeString(description);
	}
	
	
}
