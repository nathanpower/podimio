package com.toastworks.podimio.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedItemDao implements Parcelable {
	private String url;
	private String feedUrl;
	private String title;
	private String description;

	public FeedItemDao() {
	}

	private FeedItemDao(Parcel in) {
		in.writeString(url);
		in.writeString(feedUrl);
		in.writeString(title);
		in.writeString(description);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFeedUrl() {
		return feedUrl;
	}

	public void setFeedUrl(String feedUrl) {
		this.feedUrl = feedUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public FeedItemDao createFromParcel(Parcel in) {
			return new FeedItemDao(in);
		}

		public FeedItemDao[] newArray(int size) {
			return new FeedItemDao[size];
		}
	};

	@Override
	public String toString() {
		return title;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(url);
		out.writeString(feedUrl);
		out.writeString(title);
		out.writeString(description);
	}

}
