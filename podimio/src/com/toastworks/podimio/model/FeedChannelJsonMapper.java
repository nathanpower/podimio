	package com.toastworks.podimio.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedChannelJsonMapper implements Parcelable {
	private String title;
	private String description;
	private String url;
	private JSONArray itemsJson = new JSONArray();
	private ArrayList<FeedItemJsonMapper> items = new ArrayList<FeedItemJsonMapper>();
	
	public FeedChannelJsonMapper(JSONObject result) throws JSONException {
		title = result.getString("Title");
		description = result.getString("Description");
		url = result.getString("Url");
		itemsJson = result.getJSONArray("Items");	
		
		for(int i = 0; i < itemsJson.length(); i++){
			items.add(new FeedItemJsonMapper((JSONObject)itemsJson.get(i), url));
		}	
	}
	
	public FeedChannelJsonMapper(Parcel in){
		in.writeString(url);
        in.writeString(title);
        in.writeString(description);
        in.writeTypedList(items);
        
	}
	
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

	public ArrayList<FeedItemJsonMapper> getItems() {
		return items;
	}

	@Override
	public String toString() {
		return title;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(url);
        out.writeString(title);
        out.writeString(description);
        out.writeTypedList(items);
	}
	
	final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public FeedChannelJsonMapper createFromParcel(Parcel in) { return new FeedChannelJsonMapper(in); }
        public FeedChannelJsonMapper[] newArray(int size) { return new FeedChannelJsonMapper[size]; }
    };
}
	
	


