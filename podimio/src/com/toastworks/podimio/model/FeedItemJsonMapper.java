package com.toastworks.podimio.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedItemJsonMapper implements Parcelable{

	private String title;
	private String description;
	private String url;
	private String feedUrl;
	
	public FeedItemJsonMapper(JSONObject result, String feedUrl) throws JSONException {
		title = result.getString("Title");
		description = result.getString("Description");
		url = result.getString("Url");
		this.feedUrl = feedUrl;
	}
	
	public FeedItemJsonMapper(Parcel in){
		in.writeString(url);
        in.writeString(title);
        in.writeString(description);
        in.writeString(feedUrl);
        
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

	public String getFeedUrl() {
		return feedUrl;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(url);
        out.writeString(title);
        out.writeString(description);
        out.writeString(feedUrl);
	}
	
}
