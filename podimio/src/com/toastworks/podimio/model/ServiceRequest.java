package com.toastworks.podimio.model;

import java.util.List;

import org.apache.http.NameValuePair;

public class ServiceRequest {
	private RequestMethod mMethod;
	private String mUrl;
	private List<NameValuePair> mParams = null;
	

	public ServiceRequest(RequestMethod method, String url){
		mMethod = method;
		mUrl = url;
	}

	public RequestMethod getMethod() {
		return mMethod;
	}

	public String getUrl() {
		return mUrl;
	}
	
	
	
	public List<NameValuePair> getParams(){
		return mParams;
	}
	
	public void setmParams(List<NameValuePair> mParams) {
		this.mParams = mParams;
	}
}
